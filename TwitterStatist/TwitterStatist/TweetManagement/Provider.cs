﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TweetManagement
{
    public class Provider
    {
        private ReaderWriterLockSlim ProviderLock = new ReaderWriterLockSlim();
        private object syncLook = new Object();
        public string Name { get; set; }
        /// <summary>
        /// 1-ready,
        /// 2-work,
        /// 3-block
        /// </summary>
        public int Flag { get; set; }

        DateTime BlockTime { get; set; }

        public Provider()
        {

        }

        internal void CheckLimit()
        {
            throw new NotImplementedException();
        }


        //internal void ChangeFlags(Flags flag)
        //{
        //   bool wrongOperation = false;
        //    switch (flag)
        //    {
        //        case Flags.Ready:
        //            if ((Flag ==(int)Flags.Block)|| (Flag == (int)Flags.Work) ) //jeśli była work lub Block
        //                SetFlag(Flags.Ready);
        //            else
        //                wrongOperation = true;
        //            break;
        //        case Flags.Block:
        //            if (Flag == (int)Flags.Work)
        //                SetFlag(Flags.Work);
        //            else
        //                wrongOperation = true;
        //            break;
        //        case Flags.Work:
        //            if ((Flag == (int)Flags.Block) || (Flag == (int)Flags.Ready))
        //                SetFlag(Flags.Work);
        //            else
        //                wrongOperation = true;
        //            break;
        //        default:
        //            break;
        //    }
        //    if (wrongOperation) ;
        //        //Logger(Wrong change Flags {0}, {1});
        //}

        internal Provider ChangeFlag(Flags flags)
        {
            ProviderLock.EnterWriteLock();
            try
            {
                this.Flag = (int)flags;
                return this;
            }
            finally
            {
                ProviderLock.ExitWriteLock();
            }
        }

        //private void SetFlag(Flags flag)
        //{ // co w przypadku gdy nie chce juz nic asynchronicznie robić 
        //  if ( Flag!=(int)flag)
        //    {
        //        lock(syncLook)
        //        {
        //            Flag = (int)flag;
        //            Console.WriteLine("Provider " + Name + " change flag to "+ flag);
        //        }
        //    }
        //}



        public void OnNext(Provider value)
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        internal bool CheckFlag(Flags flags)
        {
            ProviderLock.EnterReadLock();
            try
            {
                return this.Flag == (int)flags;

            }
            finally
            {
                ProviderLock.EnterWriteLock();
            }
        }

        internal void BlockMe()
        {
            bool isLimit = true;
            ProviderLock.EnterWriteLock();//Blokuje aż do odwołania 
            try
            {
                while (isLimit)
                {
                    Thread.Sleep(1000); //zamienić sleep na coś nowszego
                                        //Limit ?
                    isLimit = true;//metoda sprawdzajaca limit dla danego providera
                }
                if(!isLimit)//moze bez 
                {
                    ManageProviders.Instance.ProviderIsReady(this);
                    return;
                }
            }
            finally
            {
                ProviderLock.EnterWriteLock();
            }
        }
    }
}

