﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TweetManagement
{
  public  class Downloader : IObserver<Downloader>
    {

        public string Name { get; set; }
        public Provider MyProvider { get; set; }

        private int Flag { get; set; }
        public Downloader(string name)
        {
            Name = name;
            Flag = 1;

        }

        public async Task StartAgainAsync()
        {

            LoadSaveMode(); 
            await StartAsync(); //z odpowiednimy parametrami
        }

        private void LoadSaveMode()
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync()
        {
            Console.WriteLine("Start Dwoloader " + Name);

            while (true)
            {
                bool EndWork = false;
                int LastID = 0;
                MyProvider = await Task.Factory.StartNew(() => ManageProviders.Instance.GetProvider());
                if (MyProvider == null) //nie ma porvidera 
                {
                    SaveState(); //trzeba być pewnym że się zapisał wiec sync
                    ManageProviders.Instance.Subscribe(this); //dodajemy do listy subskrybędtow i czeka na providera

                    //Wait;
                    return;

                }
                Console.WriteLine("Provider " + MyProvider.Name + " for Downloader  " + Name);

                try
                {
                DownloadTweets(MyProvider, out EndWork, out LastID);
                }
                catch
                {
                    MyProvider.CheckLimit();
                    ProviderIsBloked(); //wywałił błąd bo prawedopodonie jest limit 
                    //MyProvider = await Task.Factory.StartNew(() => ManageProviders.Instance.GetProvider());
                }
                if (EndWork)
                    return;                               
            }
        }

        private void ProviderIsBloked()
        {
            ManageProviders.Instance.ProviderIsBlocked(MyProvider);
        }

        private void SaveState()
        {
            Flag = 3;
        }


        public async void StartFromSaveAsync()
        {
           await StartAsync();
        }
        private void DownloadTweets(Provider MyPro, out bool EndWok,out int LastID)
        {
            EndWok = false;
            LastID = 100;
            Console.WriteLine("Provider " + MyProvider.Name + " for Downloader  " + Name);

          Task.Delay(1000);
        }

        public void OnNext(Downloader value)
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
    }
}