﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Threading;

namespace TweetManagement
{
    enum Flags { Ready = 1, Work, Block };
    public sealed class ManageProviders : IObservable<Downloader>
    {

        private static volatile ManageProviders instance;
        private static object syncLook = new Object();
        private static object listLook = new Object();
        private int trafic = 3; //liczba downloaderów na jednego providera
        private static object checkinglook = new Object();
        private ReaderWriterLockSlim casheLock = new ReaderWriterLockSlim();


        static IList<Provider> Providers { get; set; } // dla cashea 
        static IList<Provider> WorkProviders { get; set; }
        private IList<IObserver<Downloader>> Downloaders { get; set; } //dla iobservable

        internal Provider GetProvider() //read
        {
            Provider pr = null;
            casheLock.EnterUpgradeableReadLock(); //read , jesli znajdzie sieready to zmieniamy, jeśłi work to nie zmieniamy
            try
            {
                pr = Providers.Where(p => p.Flag == (int)Flags.Ready).FirstOrDefault();
                if (pr == null)
                {
                    pr = Providers.Where(p => p.Flag == (int)Flags.Work).FirstOrDefault(); //workMode
                    if (pr == null)
                    {
                        //IOBservalbeWaiat with SendMEssage //brak providerow , metoda ma sprawdzać czy się odblokowały i dać znać że jest, dotychczasowe sprawy maja byś skończone , dodanie downloadera do listy
                        //AddToList();                    
                        return null;
                    }
                    else
                        return pr; //zwracamy z flaga work 
                }
                else //mamy ready
                {
                    casheLock.EnterWriteLock(); //lock na liście 
                    try
                    {
                        return pr.ChangeFlag(Flags.Work);// brak syncronicznośći bo i tak czekamy na powrót 
                    }
                    finally
                    {
                        casheLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                casheLock.ExitUpgradeableReadLock();
            }
        }

        internal async void ProviderIsBlocked(Provider myProvider)
        {
            casheLock.EnterUpgradeableReadLock(); //read , jesli jeszcze provider nie zmienony to zmieniamy
            try
            {
                if (!myProvider.CheckFlag(Flags.Block)) //jeśli flaga jest taka sama return true 
                {  //jesli jeszcze zaden process mnie nie zablokował 
                    casheLock.EnterWriteLock(); //look na liscie
                    try
                    {
                        myProvider.ChangeFlag(Flags.Block); //look na providerze i powrót juz ze zmienioną flagą 
                    }
                    finally
                    {
                        casheLock.ExitWriteLock(); //opuszczam liste
                       await Task.Factory.StartNew(()=>myProvider.BlockMe()); //uruchamiam mechaniz oczekiwania na zmiane Limitu
                    }
                }
            }
            finally
            {
                casheLock.ExitUpgradeableReadLock();
            }
        }

        private ManageProviders()
        {
            Providers = new List<Provider>()
            {
                new Provider()
                {
                    Name="P1",
                    Flag=1
                },
                   new Provider(){
                    Name="P2",
                    Flag=1
                },
                   new Provider(){
                    Name="P3",
                    Flag=1
                }
            };

        }
        //odbiera informacje że dany provder jest już dostępny 
        internal void ProviderIsReady(Provider provider)
        {
            //w tym momenci dostajemy providdera który jewst gotowy do pracy, ale jeszcze nie ma flagi is redy ( żeby inny proces go nie podebrał
            casheLock.EnterWriteLock(); //bedziemy zmieniać liste
            try
            {
                provider.ChangeFlag(Flags.Ready); //LOOOK NA PROVIDERZE 
            }
            finally
            {
                casheLock.ExitWriteLock();
                LetKnowDownloaders(); // poinformuj downloadery że jest wolny provider
            }

        }

        private async void LetKnowDownloaders()
        {

            casheLock.ExitReadLock();
            try
            {
                if (Downloaders.Any())
                {
                    foreach (Downloader downloader in Downloaders.Take(Providers.Where(p => p.Flag == (int)Flags.Ready).Count() * trafic)) //wszystkim dajemy info ( do zrobinie 
                    {
                        await downloader.StartAgainAsync();//zacznij z Save mode pobierając providera /jeśli jest (3 wotki) i nie dupisuj mnie do Downloadersów
                    }
                }

            }
            finally
            {
                casheLock.ExitReadLock();
            }

            //tylko tulu ilu jest Providerów ( max 3 na 1 provider 
            //taka sama logika przy ponownym pobieraniu providera z flagą wokr
            //jeśli tylko są jakieś oczekujące downloader to ona ma piorytet na zajęcia providera 
            foreach (var downloader in Downloaders)
            {
                // downloader.StartAgain();
            }
        }

        public static ManageProviders Instance
        {
            get
            {

                if (instance == null)
                {
                    lock (syncLook)
                    {
                        if (instance == null)
                        {
                            instance = new ManageProviders();
                        }
                    }
                }
                return instance;
            }
        }

        public IDisposable Subscribe(IObserver<Downloader> downloader)
        {
            Downloaders.Add(downloader);
            StartCecking();
            return null; //Idisposable poczytać 
        }

        private void StartCecking()
        {
            lock (checkinglook) //załątwia nam singletona bo blokuje od samego początku 
            {
                TweetCheck();
            }
        }

        private void TweetCheck()
        {
            bool isLimit;
            while (Downloaders.Any())
            {
                isLimit = true;
                casheLock.EnterUpgradeableReadLock();
                try
                {
                    foreach (var provider in Providers) //foreachAsync
                    {
                        //Tweeeter sprawdx rate 
                        if (true) //jeśli znalazł (fajnie jakby szukał asynchronicznie / foreach asynck ( potwrót contextu 
                        {
                            //   flaga na Eork
                            //      Wywołanie pOnowne(samo pobierze providera)
                        }
                        break;
                    }
                }
                finally
                {
                    casheLock.ExitUpgradeableReadLock();
                }
                if (isLimit)
                    Thread.Sleep(15000);
            }

            Thread.Sleep(15000);
        } 
    

    }
}

