﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TweetManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Downloader d1 = new Downloader("D1");
            Downloader d2 = new Downloader("D2");
            Downloader d3 = new Downloader("D3");



            d1.StartAsync();
            Thread.Sleep(100);
            d3.StartAsync();
            Thread.Sleep(100);
            d2.StartAsync();

            System.Console.ReadKey();
        }
    }
}
