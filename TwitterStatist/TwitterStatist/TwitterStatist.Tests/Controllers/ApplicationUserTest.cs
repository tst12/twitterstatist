﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TwitterStatist.Models;

namespace TwitterStatist.Tests.Controllers
{
    [TestClass]
    public class ApplicationUserTest
    {
        [TestMethod]
        public void TestAddUser()
        {
            // Arrange
            var userStore = new Mock<IUserStore<Models.ApplicationUser>>();
            var userManager = new Mock<UserManager<ApplicationUser>>(userStore.Object);
            //var loginModel = new LoginViewModel
            //{
            //    Email = "myemail@test.com",
            //    Password = "b",
            //    RememberMe = false
            //};
          //  var returnUrl = "/foo";
            var user = new ApplicationUser
            {
                UserName = ""
            };
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));

            userManager.Setup(um => um.FindAsync(loginModel.Email, loginModel.Password)).Returns(Task.FromResult(user));
            userManager.Setup(um => um.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie)).Returns(Task.FromResult(identity));

            var controller = new AccountController(userManager.Object);
            var helper = new MvcMockHelper(controller);

            // Act
            var actionResult = controller.Login(loginModel, returnUrl).Result;

            // Assert
            var redirectResult = actionResult as RedirectResult;
            Assert.IsNotNull(redirectResult);
            Assert.AreEqual(returnUrl, redirectResult.Url);

            Assert.AreEqual(loginModel.Email, helper.OwinContext.Authentication.AuthenticationResponseGrant.Identity.Name);
            Assert.AreEqual(DefaultAuthenticationTypes.ExternalCookie, helper.OwinContext.Authentication.AuthenticationResponseRevoke.AuthenticationTypes.First());
        }
        public void TestMethod1()
        {



        }
    }
}
