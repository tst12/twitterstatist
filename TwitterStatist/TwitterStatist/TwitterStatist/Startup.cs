﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TwitterStatist.Startup))]
namespace TwitterStatist
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
