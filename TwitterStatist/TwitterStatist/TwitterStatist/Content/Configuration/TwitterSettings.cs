﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwitterStatist.Configuration
{
    public static class TwitterSettings
    {
        public readonly static string ConsumerKey = AppSettings.Setting<string>("consumerKey");
        public readonly static string ConsumerSecret = AppSettings.Setting<string>("consumerSecret");

        public const string AccessTokenClaimType = "urn:tokens:twitter:accesstoken";
        public const string AccessTokenSecretClaimType = "urn:tokens:twitter:accesstokensecret";
    }
}