﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using TwitterStatist.Repositores;
using TwitterStatist.Models;
using System.Threading.Tasks;

namespace TwitterStatist.Helpers
{
    public class TweetsHelper
    {
        Searcher searcher;
        private TwitterContext ctx;
        private List<SingleUserInMemoryCredentialStore> CredentialList;
        //private TwitterContext ctx;
        public TweetsHelper(TwitterContext ctx)
        {
            this.ctx = ctx;
        }
        public TweetsHelper(Searcher searcher)
        {
            this.searcher = searcher;
            SetAuth();
            SetContext(0);
        }

        private SearcherRepository _searcherRepo;

        public SearcherRepository searcherRepo
        {
            get
            {
                if (_searcherRepo == null)
                    _searcherRepo = new SearcherRepository();
                return _searcherRepo;
            }
            set
            {
                _searcherRepo = value as SearcherRepository;
            }
        }

        private TweetRepository _tweetRepo;

        public TweetRepository tweetRepo
        {
            get
            {
                if (_tweetRepo == null)
                    _tweetRepo = new TweetRepository();
                return _tweetRepo;
            }
            set
            {
                _tweetRepo = value as TweetRepository;
            }
        }

    
        private void DownloadTweetByRangeAsync(ulong startID, ulong stopID)
        {
      //    Task.Factory.StartNew(() =>new Downloader(searcher).StartAsync(startID,stopID));
        }

        private void DownloadTweetByRange(object startID, object stopID)
        {
            throw new NotImplementedException();
        }
        private ulong GetLastTweetIDByDate(DateTime dateTime)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Daj najstarszy możliwy wpis z tego dnia 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        //private ulong GetFirstTweetIDByDate(DateTime dateTime) 
        //{
        //    var searchResponse =
        //             (from search in ctx.Search
        //              where search.Type == SearchType.Search &&
        //                    search.Query == searcher.Query &&
        //                    search.Count == 10 &&
        //                       search.Until == dateTime
        //              select search.Statuses).ToList();
        //    searchResponse.
        //    var t = searchResponse.Count();
        //}
        ////ostatni tweet danego dnia 
        //unitl na date-1
        public ulong LastTweetIDByDate(DateTime dateTime)
        {
            SetContext(1);

            try
            {
                var searchResponse =
                                    (from search in ctx.Search
                                     where search.Type == SearchType.Search &&
                                           search.Query == searcher.Query &&
                                           search.Count == 5 //&&
                                          //    search.Until == dateTime.AddDays(-2)
                                     select search.Statuses).FirstOrDefault().ToList();
                var t = searchResponse.Count();
                if (searchResponse.Any())
                {
                    SetContext(1);
                    return searchResponse.First().StatusID;

                }
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        private ulong GetEldestTweetID()
        {
            searcher.Start = new DateTime(2016, 11, 5);
            var maxID = ulong.MaxValue;

            var searchResponse =
                    (from search in ctx.Search
                     where search.Type == SearchType.Search &&
                           search.Query == searcher.Query &&
                           search.Count == 130 &&
                              search.Until == searcher.Start.AddDays(-1)
                     select search.Statuses).FirstOrDefault();
            var t = searchResponse.Count();

            #region e
            //   var searchResponse6 =
            //       (from search in ctx.Search
            //        where search.Type == SearchType.Search &&
            //              search.Query == searcher.Query &&
            //              search.Count == 1 &&
            //              search.Until==new DateTime(2016,11,4)
            //        select search.Statuses).FirstOrDefault();



            //   var searchResponse4 =
            //           (from search in ctx.Search
            //            where search.Type == SearchType.Search &&
            //                  search.Query == searcher.Query &&
            //                  search.Count == 1
            //                  &&
            //                  search.SinceID == maxID
            //            select search.Statuses).FirstOrDefault();


            //   var searchResponse1 =
            //     (from search in ctx.Search
            //      where search.Type == SearchType.Search &&
            //            search.Query == searcher.Query &&
            //            search.Count == 1 &&
            //            search.SinceID == 1&&
            //            search.MaxID==maxID
            //      select search.Statuses).FirstOrDefault();

            //   var searchResponse2 =
            //     (from search in ctx.Search
            //      where search.Type == SearchType.Search &&
            //            search.Query == searcher.Query &&
            //           search.Count == 1 &&
            //            search.SinceID == maxID &&
            //            search.MaxID == 1
            //      select search.Statuses).FirstOrDefault();

            //   var searchResponse3 =
            //     (from search in ctx.Search
            //      where search.Type == SearchType.Search &&
            //            search.Query == searcher.Query &&
            //           search.Count == 1 &&
            //            search.MaxID == 1
            //      select search.Statuses).FirstOrDefault();
            //   //794211689994604544
            //          // 794689994604544
            //   maxID = 794211689994604544;
            //   var searchResponse5 =
            //(from search in ctx.Search
            // where search.Type == SearchType.Search &&
            //       search.Query == searcher.Query &&
            //      search.Count == 1 &&
            //       search.MaxID == maxID
            // select search.Statuses).FirstOrDefault();


            //  try
            //   { 
            //     var  searchRes5 =
            //           (from search in ctx.Search
            //            where search.Type == SearchType.Search &&
            //            search.Query == searcher.Query &&
            //            search.Count == 1000 &&
            //            search.MaxID == maxID
            //            select search.Statuses).ToList();
            //      var t= searchRes5.Count();
            //   }
            //   catch
            //   {

            //   }
            #endregion
            return 3;
        }

        private void SetContext(int number)
        {
            var auth = DoSingleUserAuth(CredentialList[number]);
            this.ctx = new TwitterContext(auth);
        }

        private void SetAuth()
        {    //      string oauthToken = "4776907498-fGk5HTlTcutzByVoEeUMdq5qCDkBVacPSxrIaY5";
             //    string oauthTokenSecret = "Mguhe9XLqAdCOlPoRYbj82Etlwqqqg4O8C5VZtYzMJc2K";


            CredentialList = new List<SingleUserInMemoryCredentialStore>();
            CredentialList.Add(
         new SingleUserInMemoryCredentialStore
         {
             ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
             ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
             AccessToken = "4776907498-fGk5HTlTcutzByVoEeUMdq5qCDkBVacPSxrIaY5",
             AccessTokenSecret = "Mguhe9XLqAdCOlPoRYbj82Etlwqqqg4O8C5VZtYzMJc2K"
         });

            //var auth2 = DoSingleUserAuth(cred);         
            //var twitterCtx = new TwitterContext(auth2);

            //    oauthToken = "4885249408-Ny7bEryyli5UsrbWCbMuFEg07CQp8qIpbncNVGN";
            //     oauthTokenSecret = "EQyc3YVEXhHno0mKijwsGhFEhuVtqf9dMTYIUQACULFb6";

            CredentialList.Add(
     new SingleUserInMemoryCredentialStore
     {
         ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
         ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
         AccessToken = "4885249408-Ny7bEryyli5UsrbWCbMuFEg07CQp8qIpbncNVGN",
         AccessTokenSecret = "EQyc3YVEXhHno0mKijwsGhFEhuVtqf9dMTYIUQACULFb6"
     });


            //var auth = DoSingleUserAuth(cred2);

            //var ctx = new TwitterContext(auth);
            //this.ctx = ctx;

        }

        static IAuthorizer DoSingleUserAuth(SingleUserInMemoryCredentialStore credentialStore)
        {
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = credentialStore
            };

            return auth;
        }


        //private ulong GetEldestTweet()
        //{
        //   var searchResponse =

        //           (from search in ctx.Search
        //            where search.Type == SearchType.Search &&
        //                  search.Query == searcher.Query &&
        //                  search.Count == 1
        //                //  && search.SearchLanguage
        //                    &&search.
        //                select search.Statuses).FirstOrDefault();

        //}
        internal void DeepSearch(int searchId)
        {
            var searcher = searcherRepo.GetById(searchId);
            int MaxSearchEntriesToReturn = 10;

            ulong sinceID = 1;
            ulong maxID;

            var combinedSearchResults = new List<Status>();
            List<Status> searchResponse;
            try
            {
                searchResponse =

                     (from search in ctx.Search
                      where search.Type == SearchType.Search &&
                            search.Query == searcher.Query &&
                            search.Count == MaxSearchEntriesToReturn
                            && search.SinceID == sinceID
                      select search.Statuses).FirstOrDefault();

                //   .SingleOrDefaultAsync();
                //searchResponse =
                //await
                //(from search in ctx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == searcher.Query &&
                //       search.Count == MaxSearchEntriesToReturn
                // && search.SinceID == sinceID
                // select search.Statuses).FirstOrDefaultAsync();
                ////.SingleOrDefaultAsync();


                //var searchRespons3e =
                //await
                //(from search in twitterCtx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == "Krakow"
                // select search).FirstAsync();



                //maxID = searchRespons3e.Statuses.Min(
                //                  status => status.StatusID) - 1;

                //sinceID = searchRespons3e.Statuses.Max(
                //                 status => status.StatusID);
                bool contin = false;
                combinedSearchResults.AddRange(searchResponse);
                ulong previousMaxID = ulong.MaxValue;
                do
                {
                    contin = false;
                    try
                    {
                        // one less than the newest id you've just queried
                        maxID = searchResponse.Min(status => status.StatusID) - 1;

                        Debug.Assert(maxID < previousMaxID);
                        previousMaxID = maxID;

                        //searchResponse =
                        //    await
                        //    (from search in twitterCtx.Search
                        //     where search.Type == SearchType.Search &&
                        //           search.Query == searchTerm &&
                        //           search.Count == MaxSearchEntriesToReturn &&
                        //           search.MaxID == maxID &&
                        //           search.SinceID == sinceID
                        //     select search.Statuses)
                        //    .SingleOrDefaultAsync();


                        searchResponse =
                        (from search in ctx.Search
                         where search.Type == SearchType.Search &&
                               search.Query == searcher.Query &&
                               search.Count == MaxSearchEntriesToReturn &&
                               search.MaxID == maxID &&
                               search.SinceID == sinceID
                         select search.Statuses)
                        .SingleOrDefault();


                        combinedSearchResults.AddRange(searchResponse);
                    }
                    catch (Exception ex)
                    {

                        //if (reteInfo.RateLimits.
                        var Limit = GetRateInfo();
                        var Remain = Limit.RateLimits["search"].FirstOrDefault().Remaining;
                        if (Remain == 0) //znaczy 
                                         //  this.ReteInfo();
                            ChangeProvider();

                        // SetContext();
                        contin = true;
                    }

                }
                while ((searchResponse.Any() || contin) && combinedSearchResults.Count <= 10);

            }
            catch (Exception ex)
            {
                this.ReteInfo();
            }



            //Zapis co 100 
            var list = ConvertStatus2Tweet(combinedSearchResults, searcher.Id);

            tweetRepo.Add(list[0]);
            //tweetRepo.AddRange(ConvertStatus2Tweet(combinedSearchResults, searcher.Id));
        }


        //StringBuilder sb = new StringBuilder();
        //combinedSearchResults.ForEach(tweet =>
        //     sb.AppendLine(string.Format(
        //        "\n {0}--{1}--{2}  User: {3} ({4})\n  Tweet: {5}",
        //        tweet.StatusID,
        //        tweet.Lang,
        //        tweet.CreatedAt,
        //        tweet.User.ScreenNameResponse,
        //        tweet.User.UserIDResponse,
        //        tweet.Text)));
        //var tt = sb.ToString();
        //if (!string.IsNullOrEmpty(tt))
        //{
        //    using (StreamWriter outputFile = new StreamWriter(@"C:\TEST\Twitter.txt"))
        //    {
        //        outputFile.Write(tt);
        //    }


        //combinedSearchResults.ForEach(tweet => new TweetViewModel
        ////            {
        ////                ImageUrl = tweet.User.ProfileImageUrl,
        ////                ScreenName = tweet.User.ScreenNameResponse,
        ////                Text = tweet.Text
        ////            }).ToList()



        //    "\n {0}--{1}--{2}  User: {3} ({4})\n  Tweet: {5}",
        //            tweet.StatusID,
        //            tweet,
        //            tweet.CreatedAt,
        //            tweet.User.ScreenNameResponse,
        //            tweet.User.UserIDResponse,
        //            tweet.Text)));
        //}

        private List<Tweet> ConvertStatus2Tweet(List<Status> ListStatus, int searcherId)
        {
            return ListStatus.Select(
                s => CreateTweet(s, searcherId)).ToList();
        }

        private Tweet CreateTweet(Status s, int searcherId)
        {

            return new Tweet
            {
                TweeterID = s.StatusID, //to DO
                Date = s.CreatedAt,
                Text = s.Text,
                Author = s.User.ScreenNameResponse + " " + s.User.UserIDResponse, //to do , polityka z userami z tweet
                SearcherID = searcherId,
            };
        }

        private int MapLand(string lang)
        {
            return 2; //TO DO
        }

        private void ChangeProvider()
        {
            SetContext(0);
        }

        private object await(IQueryable<List<Status>> queryable)
        {
            throw new NotImplementedException();
        }

        //public void DoPagedSearchByStream()
        //{
        //    const int MaxSearchEntriesToReturn = 100;

        //    string searchTerm = "nato";

        //    // oldest id you already have for this search term
        //    //ulong sinceID = 770869254312845311;

        //    ulong sinceID = 1;

        //    // used after the first query to track current session
        //    ulong maxID;

        //    var combinedSearchResults = new List<Status>();
        //    List<Status> searchResponse;
        //    try
        //    {

        //        searchResponse =
        //        (from strm in ctx.Streaming
        //         where strm.Type==StreamingType.Filter &&
        //         strm.Track==searchTerm
        //         && strm.s

        //         )



        //   (from strm in twitterCtx.Streaming
        //    where strm.Type == StreamingType.Filter &&
        //          strm.Track == "twitter"
        //    select strm)
        //   .StartAsync(async strm =>
        //   {
        //       Console.WriteLine(strm.Content + "\n");

        //       if (count++ >= 5)
        //           strm.CloseStream();
        //   });

        //        searchResponse =

        //             (from search in ctx.Search
        //              where search.Type == SearchType.Search &&
        //                    search.Query == searchTerm &&
        //                    search.Count == MaxSearchEntriesToReturn
        //                    && search.SinceID == sinceID
        //              select search.Statuses).FirstOrDefault();

        //        //.SingleOrDefaultAsync();           
        //        //searchResponse =
        //        //await
        //        //(from search in twitterCtx.Search
        //        // where search.Type == SearchType.Search &&
        //        //       search.Query == searchTerm &&
        //        //       search.Count == MaxSearchEntriesToReturn
        //        //     //&& search.SinceID == sinceID
        //        //     select search.Statuses).FirstOrDefaultAsync();
        //        ////.SingleOrDefaultAsync();


        //        //var searchRespons3e =
        //        //await
        //        //(from search in twitterCtx.Search
        //        // where search.Type == SearchType.Search &&
        //        //       search.Query == "Krakow"
        //        // select search).FirstAsync();



        //        //maxID = searchRespons3e.Statuses.Min(
        //        //                  status => status.StatusID) - 1;

        //        //sinceID = searchRespons3e.Statuses.Max(
        //        //                 status => status.StatusID);

        //        combinedSearchResults.AddRange(searchResponse);
        //        ulong previousMaxID = ulong.MaxValue;
        //        do
        //        {
        //            // one less than the newest id you've just queried
        //            maxID = searchResponse.Min(status => status.StatusID) - 1;

        //            Debug.Assert(maxID < previousMaxID);
        //            previousMaxID = maxID;

        //searchResponse =
        //    await
        //    (from search in twitterCtx.Search
        //     where search.Type == SearchType.Search &&
        //           search.Query == searchTerm &&
        //           search.Count == MaxSearchEntriesToReturn &&
        //           search.MaxID == maxID &&
        //           search.SinceID == sinceID
        //     select search.Statuses)
        //    .SingleOrDefaultAsync();


        //            searchResponse =
        //            (from search in ctx.Search
        //             where search.Type == SearchType.Search &&
        //                   search.Query == searchTerm &&
        //                   search.Count == MaxSearchEntriesToReturn &&
        //                   search.MaxID == maxID &&
        //                   search.SinceID == sinceID
        //             select search.Statuses)
        //            .SingleOrDefault();


        //            combinedSearchResults.AddRange(searchResponse);
        //        }
        //        while (searchResponse.Any());

        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    StringBuilder sb = new StringBuilder();
        //    combinedSearchResults.ForEach(tweet =>
        //         sb.AppendLine(string.Format(
        //            "\n {0}--{1}--{2}  User: {3} ({4})\n  Tweet: {5}",
        //            tweet.StatusID,
        //            tweet.Lang,
        //            tweet.CreatedAt,
        //            tweet.User.ScreenNameResponse,
        //            tweet.User.UserIDResponse,
        //            tweet.Text)));
        //    var tt = sb.ToString();
        //    if (!string.IsNullOrEmpty(tt))
        //    {
        //        using (StreamWriter outputFile = new StreamWriter(@"C:\TEST\Twitter.txt"))
        //        {
        //            outputFile.Write(tt);
        //        }

        //    }
        //    //combinedSearchResults.ForEach(tweet => new TweetViewModel
        //    ////            {
        //    ////                ImageUrl = tweet.User.ProfileImageUrl,
        //    ////                ScreenName = tweet.User.ScreenNameResponse,
        //    ////                Text = tweet.Text
        //    ////            }).ToList()



        //}


        public Help GetRateInfo()
        {
            var helpResult =
             (from help in ctx.Help
              where help.Type == HelpType.RateLimits &&
                                                     help.Resources == "search"
              select help)
             .SingleOrDefault();
            return helpResult;

        }

        public void ReteInfo()
        {

            StringBuilder info = new StringBuilder();
            var helpResult =
       (from help in ctx.Help
        where help.Type == HelpType.RateLimits &&
                                               help.Resources == "search"
        select help)
       .SingleOrDefault();

            foreach (var category in helpResult.RateLimits)
            {
                Console.WriteLine("\nCategory: {0}", category.Key);

                foreach (var limit in category.Value)
                {

                    info.AppendLine(string.Format("\n  Resource: {0}\n    Remaining: {1}\n    Reset: {2}\n    Limit: {3}",
                        limit.Resource, limit.Remaining, limit.Reset, limit.Limit));
                }
            }

            var tt = info.ToString();

        }

        public void DoPagedSearch(string searchTerm)
        {
            int MaxSearchEntriesToReturn = 1000;
            searchTerm = "nato";
            ulong sinceID = 1;
            ulong maxID;

            var combinedSearchResults = new List<Status>();
            List<Status> searchResponse;
            try
            {
                searchResponse =

                     (from search in ctx.Search
                      where search.Type == SearchType.Search &&
                            search.Query == searchTerm &&
                            search.Count == MaxSearchEntriesToReturn
                            && search.SinceID == sinceID
                      select search.Statuses).FirstOrDefault();

                //.SingleOrDefaultAsync();           
                //searchResponse =
                //await
                //(from search in twitterCtx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == searchTerm &&
                //       search.Count == MaxSearchEntriesToReturn
                //     //&& search.SinceID == sinceID
                //     select search.Statuses).FirstOrDefaultAsync();
                ////.SingleOrDefaultAsync();


                //var searchRespons3e =
                //await
                //(from search in twitterCtx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == "Krakow"
                // select search).FirstAsync();



                //maxID = searchRespons3e.Statuses.Min(
                //                  status => status.StatusID) - 1;

                //sinceID = searchRespons3e.Statuses.Max(
                //                 status => status.StatusID);
                bool contin = false;
                combinedSearchResults.AddRange(searchResponse);
                ulong previousMaxID = ulong.MaxValue;
                do
                {
                    contin = false;
                    try
                    {
                        // one less than the newest id you've just queried
                        maxID = searchResponse.Min(status => status.StatusID) - 1;

                        Debug.Assert(maxID < previousMaxID);
                        previousMaxID = maxID;

                        //searchResponse =
                        //    await
                        //    (from search in twitterCtx.Search
                        //     where search.Type == SearchType.Search &&
                        //           search.Query == searchTerm &&
                        //           search.Count == MaxSearchEntriesToReturn &&
                        //           search.MaxID == maxID &&
                        //           search.SinceID == sinceID
                        //     select search.Statuses)
                        //    .SingleOrDefaultAsync();


                        searchResponse =
                        (from search in ctx.Search
                         where search.Type == SearchType.Search &&
                               search.Query == searchTerm &&
                               search.Count == MaxSearchEntriesToReturn &&
                               search.MaxID == maxID &&
                               search.SinceID == sinceID
                         select search.Statuses)
                        .SingleOrDefault();


                        combinedSearchResults.AddRange(searchResponse);
                    }
                    catch (Exception ex)
                    {

                        //if (reteInfo.RateLimits.
                        var Limit = GetRateInfo();
                        var Remain = Limit.RateLimits["search"].FirstOrDefault().Remaining;

                        //  this.ReteInfo();
                        SetContext(0);
                        contin = true;
                    }

                }
                while (searchResponse.Any() || contin);

            }
            catch (Exception ex)
            {

                this.ReteInfo();
            }

            StringBuilder sb = new StringBuilder();
            combinedSearchResults.ForEach(tweet =>
                 sb.AppendLine(string.Format(
                    "\n {0}--{1}--{2}  User: {3} ({4})\n  Tweet: {5}",
                    tweet.StatusID,
                    tweet.Lang,
                    tweet.CreatedAt,
                    tweet.User.ScreenNameResponse,
                    tweet.User.UserIDResponse,
                    tweet.Text)));
            var tt = sb.ToString();
            if (!string.IsNullOrEmpty(tt))
            {
                using (StreamWriter outputFile = new StreamWriter(@"C:\TEST\Twitter.txt"))
                {
                    outputFile.Write(tt);
                }

            }
            //combinedSearchResults.ForEach(tweet => new TweetViewModel
            ////            {
            ////                ImageUrl = tweet.User.ProfileImageUrl,
            ////                ScreenName = tweet.User.ScreenNameResponse,
            ////                Text = tweet.Text
            ////            }).ToList()



        }







        public void DoPagedSearch()
        {
            int MaxSearchEntriesToReturn = 1000;

            string searchTerm = "nato";

            DoPagedSearch("nato");
            // oldest id you already have for this search term
            //ulong sinceID = 770869254312845311;

            ulong sinceID = 1;

            // used after the first query to track current session
            ulong maxID;

            var combinedSearchResults = new List<Status>();
            List<Status> searchResponse;
            try
            {
                searchResponse =

                     (from search in ctx.Search
                      where search.Type == SearchType.Search &&
                            search.Query == searchTerm &&
                            search.Count == MaxSearchEntriesToReturn
                            && search.SinceID == sinceID
                      select search.Statuses).FirstOrDefault();

                //.SingleOrDefaultAsync();           
                //searchResponse =
                //await
                //(from search in twitterCtx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == searchTerm &&
                //       search.Count == MaxSearchEntriesToReturn
                //     //&& search.SinceID == sinceID
                //     select search.Statuses).FirstOrDefaultAsync();
                ////.SingleOrDefaultAsync();


                //var searchRespons3e =
                //await
                //(from search in twitterCtx.Search
                // where search.Type == SearchType.Search &&
                //       search.Query == "Krakow"
                // select search).FirstAsync();



                //maxID = searchRespons3e.Statuses.Min(
                //                  status => status.StatusID) - 1;

                //sinceID = searchRespons3e.Statuses.Max(
                //                 status => status.StatusID);

                combinedSearchResults.AddRange(searchResponse);
                ulong previousMaxID = ulong.MaxValue;
                do
                {
                    // one less than the newest id you've just queried
                    maxID = searchResponse.Min(status => status.StatusID) - 1;

                    Debug.Assert(maxID < previousMaxID);
                    previousMaxID = maxID;

                    //searchResponse =
                    //    await
                    //    (from search in twitterCtx.Search
                    //     where search.Type == SearchType.Search &&
                    //           search.Query == searchTerm &&
                    //           search.Count == MaxSearchEntriesToReturn &&
                    //           search.MaxID == maxID &&
                    //           search.SinceID == sinceID
                    //     select search.Statuses)
                    //    .SingleOrDefaultAsync();


                    searchResponse =
                    (from search in ctx.Search
                     where search.Type == SearchType.Search &&
                           search.Query == searchTerm &&
                           search.Count == MaxSearchEntriesToReturn &&
                           search.MaxID == maxID &&
                           search.SinceID == sinceID
                     select search.Statuses)
                    .SingleOrDefault();


                    combinedSearchResults.AddRange(searchResponse);
                }
                while (searchResponse.Any());

            }
            catch (Exception ex)
            {

                this.ReteInfo();
            }

            StringBuilder sb = new StringBuilder();
            combinedSearchResults.ForEach(tweet =>
                 sb.AppendLine(string.Format(
                    "\n {0}--{1}--{2}  User: {3} ({4})\n  Tweet: {5}",
                    tweet.StatusID,
                    tweet.Lang,
                    tweet.CreatedAt,
                    tweet.User.ScreenNameResponse,
                    tweet.User.UserIDResponse,
                    tweet.Text)));
            var tt = sb.ToString();
            if (!string.IsNullOrEmpty(tt))
            {
                using (StreamWriter outputFile = new StreamWriter(@"C:\TEST\Twitter.txt"))
                {
                    outputFile.Write(tt);
                }

            }
            //combinedSearchResults.ForEach(tweet => new TweetViewModel
            ////            {
            ////                ImageUrl = tweet.User.ProfileImageUrl,
            ////                ScreenName = tweet.User.ScreenNameResponse,
            ////                Text = tweet.Text
            ////            }).ToList()



        }





    }
}