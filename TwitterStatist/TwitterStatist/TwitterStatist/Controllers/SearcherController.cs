﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TwitterStatist.Models;
using TwitterStatist.Repositores;
using Microsoft.AspNet.Identity;
using TwitterStatist.Helpers;
using PagedList;
using TwitterStatist.TweetManagement;

namespace TwitterStatist.Controllers
{
    

    public class SearcherController : Controller
    {
        private int randomCount = 15;
        public UserRepository _userRepo;
        private UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }
        private SearcherRepository _searcherRepo;
        public SearcherRepository searcherRepo
        {
            get
            {
                if (_searcherRepo == null)
                    _searcherRepo = new SearcherRepository();
                return _searcherRepo;
            }
            set
            {
                _searcherRepo = value as SearcherRepository;
            }
        }
        private TwitterStatistContext db = new TwitterStatistContext();

        // GET: Searcher
        [Authorize]
        public async Task<ActionResult> Index()
        {
            User.Identity.GetUserId();
            //var searchers = db.Searchers.Include(s => s.ApplicationUser);
            ViewBag.Description = " Lista przedstawia searchery utworzone przez Ciebie.";

            return View(searcherRepo.GetAllSearchVMByUser(User.Identity.GetUserId()));
        }
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult> SearcherForUser(string id)
        {
            return View("Index",searcherRepo.GetAllSearchVMByUser(id));
        }


        /// <summary>
        /// Metoda zwraca losową liczbę searcherów
        /// </summary>
        /// <returns></returns>
        public ActionResult RandomSearchers()
        {
            ViewBag.Title = "Losowe wyniki";
            ViewBag.Discribe = "Lista przedstawia losowo wybrane wyniki wyszukiwań.";

          return View("Index",searcherRepo.GetRandomSearchVM(randomCount));
        }


        public ViewResult TweetList(int? id,string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var tweets = searcherRepo.GetTweetBySearcherID(id.Value);
            if (!String.IsNullOrEmpty(searchString))
            {
                tweets = tweets.Where(s => s.Text.Contains(searchString)
                                       || s.Author.Contains(searchString)).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tweets = tweets.OrderByDescending(s => s.Author).ToList();
                    break;
                case "Date":
                    tweets = tweets.OrderBy(s => s.Date).ToList(); ;
                    break;
                case "date_desc":
                    tweets = tweets.OrderByDescending(s => s.Date).ToList(); 
                    break;
                default:  // Name ascending 
                    tweets = tweets.OrderBy(s => s.Date).ToList(); 
                    break;
            }

            int pageSize = 30;
            int pageNumber = (page ?? 1);

           return View(tweets.ToPagedList(pageNumber, pageSize));
        }
                    
        // GET: Searcher/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var searcher = searcherRepo.GetSearcherVModel(id);
            if (searcher == null)
            {
                return HttpNotFound();
            }
            var userId = User.Identity.GetUserId();
            ViewBag.IsAuthor = (userId != null)? searcher.IdUser == userId : false;
            return View(searcher);
        }



        // GET: Searcher/Create
        [HttpGet]
        public ActionResult Create()
        {
                  return View();
        }

        private ulong GetUnixDateTime(DateTime date)
        {
            DateTime unixStart = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);
            return (ulong)Math.Floor((date.ToUniversalTime() - unixStart).TotalMilliseconds);
        }
        public JsonResult GetStatistic(int searcherId)
        {

            var TweetList = searcherRepo.GetTweetBySearcherID(searcherId);
            //var query = TweetList.AsEnumerable()
            //    .GroupBy(row => new
            //    {
            //        Date = row.Date.Date,
            //        Hour = row.Date.Date.Hour
            //    })
            //    .Select(grp => new
            //    {
            //        Date = grp.Key.Date,
            //        Hour = grp.Key.Hour,
            //        Count = grp.Count()
            //    });


            var hourGroups = TweetList.AsEnumerable()
    .Select(row => new { DateAndTime = row.Date, Row = row })
    .GroupBy(x => new { DateT = x.DateAndTime.Date, Hour = x.DateAndTime.Date.Hour });

            List<Stat> lstStat = new List<Stat>();

            foreach (var x in hourGroups)
            {

                var Day = x.GroupBy(d => new { Hour = d.DateAndTime.Hour });

                for (int i = 0; i < 24; i++)
                {
                    var HourDay = Day.Where(dh => dh.Key.Hour == i).FirstOrDefault();
                    if (HourDay != null)
                    {
                        lstStat.Add(new Stat(
                            GetUnixDateTime(x.Key.DateT.Date.AddHours(HourDay.Key.Hour)),
                            HourDay.Count()
                            ));
                    }
                    else
                    {
                        lstStat.Add(new Stat(
                                        GetUnixDateTime(x.Key.DateT.Date.AddHours(i)),
                                        0
                                     ));
                    }

                }
            }

                var rt = lstStat.Count();
            lstStat = lstStat.OrderBy(x => x.Date).ToList();

            var t = Json(lstStat, JsonRequestBehavior.AllowGet);
                return t;


            }

        public ActionResult Maps()
        {
            return View();
        }
        // POST: Searcher/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]


        public async Task<ActionResult> Create([Bind(Include = "Name,Query,Start, Latitude,Longitude,Distance, LocationValue")] SearcherViewModel searcher) //
        {
            if (ModelState.IsValid)
            {
                CheckLocation(ref searcher);
                searcherRepo.AddSearcherMVToUser(searcher, User.Identity.GetUserId());
                return RedirectToAction("Index");
            }
            return View(searcher);
        }


        // GET: Searcher/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Searcher searcher = await db.Searchers.FindAsync(id);
            if (searcher == null)
            {
                return HttpNotFound();
            }
            return View(searcher);
        }

        // POST: Searcher/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,IdUser,Name,IdWorkMode,Query,IdQueryMode,IdLand,Start,Precision")] Searcher searcher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(searcher).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //    ViewBag.IdUser = new SelectList(db.ApplicationUsers, "Id", "Email", searcher.IdUser);
            return View(searcher);
        }

        // GET: Searcher/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Searcher searcher = await db.Searchers.FindAsync(id);
            if (searcher == null)
            {
                return HttpNotFound();
            }
            return View(searcher);
        }

        //Get Searcher/ChangeWorkMode 
        public async Task<ActionResult> ChangeWorkMode(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Searcher searcher = await db.Searchers.FindAsync(id);
            if (searcher == null)
            {
                return HttpNotFound();
            }
            return View(searcher);
        }

        private void CheckLocation(ref SearcherViewModel searcher)
        {
            if (!searcher.LocationValue)
            {
                searcher.Latitude = string.Empty;
                searcher.Longitude = string.Empty;
                searcher.Distance = string.Empty;
            }
        }

        //Get Searcher/StartSearcher 
        public async Task<ActionResult> StartSearcher(int? searcherId)
        {
            if (!searcherId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var searcher = SetWorkMode(searcherId.Value, SearchWorkMode.Search);
            var tr = searcher.Id;
                await Task.Factory.StartNew(() => new Downloader(searcher).ManageWorkAsyc());
           return RedirectToAction("Details", new { id = tr });
          //  return RedirectToAction("Index");

        }
        //Get Searcher/StopSearcher 

        public async Task<ActionResult> StopSearcher(int? searcherId)
        {
            if (!searcherId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var searcher = SetWorkMode(searcherId.Value, SearchWorkMode.Stop);
            ManageCycleDownload.Instance.Dellete(searcher);

            return RedirectToAction("Details", new { id = searcher.Id });
        }


        private Searcher SetWorkMode(int searcherId, SearchWorkMode WM)
        {
            return searcherRepo.ChengeWModeTo(searcherId, (int)WM);
        }

        // POST: Searcher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Searcher searcher = await db.Searchers.FindAsync(id);
            db.Searchers.Remove(searcher);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Wykres()
        {
            return View();
        }
    }
}
