﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Web.WebPages.Html;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TwitterStatist.Models;
using System.Collections.Generic;
using TwitterStatist.Configuration;
using TwitterStatist.Repositores;
using System.Data.Entity;
using System.Reflection;
using log4net;

namespace TwitterStatist.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ILog _logger;
        private UserRepository _userRepo;
               
        private UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }

        public AccountController()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            // userRepo = new UserRepository();
        }

        public AccountController(UserManager<ApplicationUser> @object)
        {
            this.@object = @object;
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ??
                    HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    await StoreAuthTokenClaims();
                    // CheckToken();
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        public async Task<ActionResult> UserInfo(string Id)
        {
            var user = userRepo.GetById(Id);
            return View(userRepo.GetUserInfoVModel(user));
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }
        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [Authorize(Roles = "Admin")]
        //[ValidateAntiForgeryToken]
        public ViewResult UserList()
        {
            var ulist = userRepo.GetAllUsersInfoVModel();
            return View(ulist);
        }
     
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        // GET: /Account/CheckTokens
        [AllowAnonymous]
        public async Task<ActionResult> SaveTokens(string returnUrl)
        {
            var currentUserId = User.Identity.GetUserId();
            var user = userRepo.GetById(currentUserId);
            await StoreAuthTokenClaims(user);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> BlockUser(string id)
        {
            userRepo.SetBlock(id, true);
            return RedirectToAction("UserList");

        }
        public async Task<ActionResult> UnBlockUser(string id)
        {
            userRepo.SetBlock(id, false);
            return RedirectToAction("UserList");
        }
        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //return RedirectToAction("CheckTokens");
                    return RedirectToAction("UpdateTokens");
                //    return CheckTokens(loginInfo);//przekierowanie na strone z parametrem LoginInfo
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //   [HttpPost]
        [AllowAnonymous]
        //   [ValidateAntiForgeryToken]
        public ActionResult UpdateTokens()
        {
            CheckUpdateToken();       //wymagany jest request bo inaczej identyy nie uzyska Current UserID , nie opisywać
            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/ExternalLoginConfirmation //pierwsze logowania uzytkowniak, musi potwierdzić mail
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync(); //Default UserName
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser
                {
                    UserName = info.DefaultUserName,
                    Email = model.Email,
                    FirstLogin = DateTime.Now,
                    LastLogin = DateTime.Now
                };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await StoreAuthTokenClaims(user);
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToAction("UpdateTokens");
                        //   return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";
        private UserManager<ApplicationUser> @object;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Metoda sprawdza czy zapisany token jest zgodny z tym który został aktualnie uzyskany
        /// </summary>
        private void CheckUpdateToken()
        {
            string accesstokenSecret = string.Empty;
            string accesstoken = string.Empty;
            var currentUserId = User.Identity.GetUserId();

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (claims != null)
            {
                accesstoken = claims
                   .Where(c => c.Type.StartsWith("urn:tokens:twitter:accesstoken")).First().Value;

                accesstokenSecret = claims
                                .Where(c => c.Type.StartsWith("urn:tokens:twitter:accesstokensecret")).First().Value;

                if (!string.IsNullOrEmpty(accesstoken) && !string.IsNullOrEmpty(accesstokenSecret))
                {
                    AccessToken newAt = new AccessToken
                    {
                        LastUse = DateTime.Today,
                        SecretTokenAccess = accesstoken,
                        TokenAccess = accesstokenSecret
                    };
                    try
                    {
                        userRepo.UpdateAccessToken(newAt, currentUserId);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Problem to update Access tocken.");
                        _logger.Error(ex.StackTrace);
                    }

                }

            }

        }

        /// <summary>
        /// Method try to Store Twitter Claims Token.
        /// Source: http://www.jerriepelser.com/blog/get-the-twitter-profile-image-using-the-asp-net-identity
        /// 
        /// Twitter initialization is created in <see cref="Startup"/>
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task StoreAuthTokenClaims(ApplicationUser user = null)
        {
            var info = await AuthenticationManager.GetExternalLoginInfoAsync();


            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            ClaimsIdentity claimsIdentity =
            await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);

            if (claimsIdentity != null)
            {
                // Retrieve the existing claims
                var currentClaims = await UserManager.GetClaimsAsync(user.Id);

                // Get the list of access token related claims from the identity
                var tokenClaims = claimsIdentity.Claims
                    .Where(c => c.Type.StartsWith("urn:tokens:")).ToList();

                // Save the access token related claims
                foreach (var tokenClaim in tokenClaims)
                {
                    if (!currentClaims.Contains(tokenClaim))
                    {
                        await UserManager.AddClaimAsync(user.Id, tokenClaim);
                    }
                }


                await DownloadTwitterProfileImage(user.Id);
            }
        }

        /// <summary>
        /// Source: http://www.jerriepelser.com/blog/get-the-twitter-profile-image-using-the-asp-net-identity
        /// https://github.com/jerriep/AspNetIdentitySocialProfileImage/blob/master/Controllers/AccountController.cs
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task DownloadTwitterProfileImage(string userId)
        {
            //var identity = (ClaimsIdentity)User.Identity;
            //IEnumerable<Claim> claims = identity.Claims;

            //if (claims != null)
            //{
            //    // Retrieve the twitter access token and claim
            //    var accessTokenClaim = claims.FirstOrDefault(x => x.Type == TwitterSettings.AccessTokenClaimType);
            //    var accessTokenSecretClaim = claims.FirstOrDefault(x => x.Type == TwitterSettings.AccessTokenSecretClaimType);

            //    if (accessTokenClaim != null && accessTokenSecretClaim != null)
            //    {
            //        // Initialize the Twitter client
            //        var service = new TwitterService(
            //            TwitterSettings.ConsumerKey,
            //            TwitterSettings.ConsumerSecret,
            //            accessTokenClaim.Value,
            //            accessTokenSecretClaim.Value
            //        );

            //        var profile = service.GetUserProfile(new GetUserProfileOptions());
            //        // Try to download user image.
            //        if (profile != null && !String.IsNullOrWhiteSpace(profile.ProfileImageUrlHttps))
            //        {
            //            string filename = Server.MapPath(string.Format("~/ProfileImages/{0}.jpeg", userId));
            //            await DownloadProfileImage(new Uri(profile.ProfileImageUrlHttps), filename);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Download image.
        /// </summary>
        /// <param name="url">Image location.</param>
        /// <param name="filename">Image filename</param>
        /// <returns></returns>
        private async Task DownloadProfileImage(Uri url, string filename)
        {
            try
            {
                var httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.GetAsync(url);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (var fileStream = new FileStream(filename, FileMode.Create))
                    {
                        await response.Content.CopyToAsync(fileStream);
                        fileStream.Close();
                    }
                }
            }
            catch (Exception exception)
            {

            }
        }
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}