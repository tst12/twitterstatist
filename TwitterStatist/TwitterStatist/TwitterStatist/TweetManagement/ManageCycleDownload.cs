﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using TwitterStatist.Models;
using TwitterStatist.Repositores;

namespace TwitterStatist.TweetManagement
{

    public class ManageCycleDownload
    {
        private ILog _logger;
        private UserRepository _userRepo;
        private UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }
        DateTime stopCycleTime=DateTime.Now;

        private SearcherRepository _searcherRepo;
        private SearcherRepository searcherRepo
        {
            get
            {
                if (_searcherRepo == null)
                    _searcherRepo = new SearcherRepository();
                return _searcherRepo;
            }
            set
            {
                _searcherRepo = value;
            }
        }


        private static object syncLook = new Object();  //Singleton dla ManageProviders

        private ReaderWriterLockSlim SearchersLoock = new ReaderWriterLockSlim(); //lock na liste Providerów 

        private List<Searcher> Searchers { get; set; }
        private static volatile ManageCycleDownload instance;
        private System.Timers.Timer oTimer = new System.Timers.Timer();
        private double LastInterval = 0;
        private ManageCycleDownload()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            Searchers = new List<Searcher>();
            _logger.Info("ManageCycleDownload created");
            IntrvalDownloadTweet();
        }
   
        public static ManageCycleDownload Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLook)
                    {
                        if (instance == null)
                        {
                            instance = new ManageCycleDownload();
                        }
                    }
                }
                return instance;
            }
        }

        public void ChangeWorkMode(int searcherId, SearchWorkMode WM)
        {
             searcherRepo.ChengeWModeTo(searcherId, (int)WM);
        }


        //Method++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        /// <summary>
        /// Dodaje searchera do listy obsługiwanych providerów. ( w liście znajduje się tylko jeden obsługiwany Searcher
        /// </summary>
        /// <param name="searcher"></param>     
        internal void Add(Searcher searcher)
        {
            _logger.Error("Add new Searcher to List ");
            SearchersLoock.EnterWriteLock();
            try
            {
                if (!Searchers.Contains(searcher))
                {
                    if (Searchers.Count() == 0)
                        oTimer.Interval = 60000;
                    Searchers.Add(searcher);
                    ChangeWorkMode(searcher.Id, SearchWorkMode.Follow);
                }
                    
            }
            catch (Exception ex)
            {
                _logger.Error("Error with adding new Searcher " + ex.Message);
                _logger.Error(ex.StackTrace);
            }
            finally
            {
                SearchersLoock.ExitWriteLock();
            }
        }
         
        internal void Dellete(Searcher searcher)
        {
            _logger.Info("Removing Searcher from List ");
            SearchersLoock.EnterWriteLock();
            try
            {
                if (Searchers.Contains(searcher))
                {
                    Searchers.Remove(searcher);
                    oTimer.Interval = 60000 * 15;
                }
                    
            }
            catch (Exception ex)
            {
                _logger.Error("Error with removing new Searcher " + ex.Message);
                _logger.Error(ex.StackTrace);
            }
            finally
            {
                SearchersLoock.ExitWriteLock();
            }
        }
        
        private bool IsLimit { get; set; }

        internal void IntrvalDownloadTweet()
        {
            oTimer.Elapsed +=  new ElapsedEventHandler(IntervalDonwloadTweet);
            oTimer.Interval = (60000*15);//16 min- 1000 000, 2 min
            oTimer.Enabled = true;
          }

        private void IntervalDonwloadTweet(object oSource, ElapsedEventArgs oElapsedEventArgs)
       {
            DateTime startTime= DateTime.Now;
            DateTime stopTime;
            SearchersLoock.EnterReadLock();
            try
            {
                if (startTime < stopCycleTime) //rozpoczęcie było wcześniej niż zakończenie 
                    return;
                List<Task<bool>> lstD = new List<Task<bool>>();
                foreach (var search in Searchers)
                {
                    lstD.Add(new Downloader(search, 1).DownloadCycleAsync());
                }
              Task.WaitAll(lstD.ToArray());
            }
            catch (Exception ex)
            {
                _logger.Error("Error Cycle Downloading  " + ex.Message);
                _logger.Error(ex.StackTrace);
            }
            finally
            {
                stopTime = DateTime.Now;
                TimeSpan dif = stopTime - startTime;
                dif = (dif.TotalMilliseconds < 100)? new TimeSpan(0,0,0,1) : dif;
                 oTimer.Interval = SetInterval(oTimer.Interval,dif.TotalMilliseconds, Searchers.Count());
                if (!(startTime < stopCycleTime))
                    stopCycleTime = DateTime.Now;
                SearchersLoock.ExitReadLock();

            }
        }

        private double SetInterval(double interval,double totalMilliseconds, int v)
        {
            if (totalMilliseconds >= interval) //dłużej niż założono 
            {
                return interval + (totalMilliseconds - interval) * 2;
            }
            else
                return interval - (interval-totalMilliseconds) * 0.5;
        }

       
    }
}