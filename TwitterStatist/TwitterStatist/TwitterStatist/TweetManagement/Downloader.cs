﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TwitterStatist.Models;
using TwitterStatist.Repositores;
using System.Linq;
using TwitterStatist.Helpers;
using log4net;
using System.Reflection;
using System.Diagnostics;
using TwitterStatist.TweetManagement;

namespace TwitterStatist
{
    public class Downloader : IObserver<Provider>
    {
        public TwitterContext ctx;
        private Provider _Provider;
        public Provider MyProvider
        {
            get
            {

                return _Provider;
            }
            set
            {
                _Provider = value;
                SetContext();
            }
        }

        private Searcher MySearcher { get; set; }
        private TweetRepository _tweetRepo;
        private ulong SinceID { get; set; }  //nie inicjalizowane dla Manage
        private ulong MaxID { get; set; }
        private ILog _logger;
        int tweetCount = 10;
        short DownloaderCount;
        public TweetRepository tweetRepo
        {
            get
            {
                if (_tweetRepo == null)
                    _tweetRepo = new TweetRepository();
                return _tweetRepo;
            }
            set
            {
                _tweetRepo = value as TweetRepository;
            }
        }

        private const int MaxSearchEntriesToReturn = 100;
        private double MaxDay;
        public Downloader(Searcher searcher)
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            MySearcher = searcher;
        }

        public Downloader(Searcher searcher, ulong startID, ulong stopID) : this(searcher)
        {
            this.SinceID = startID;
            this.MaxID = stopID;

            //  DownloadTweetByRangeAsync();
        }
        /// <summary>
        /// For Cycling work
        /// </summary>
        /// <param name="searcher"></param>
        /// <param name="startID"></param>
        public Downloader(Searcher searcher, ulong startID) : this(searcher)
        {
            this.SinceID = startID;
        }


        #region Auth tweetContext
        private void SetContext()
        {
            if (MyProvider != null)
            {
                var auth = new SingleUserAuthorizer
                {
                    CredentialStore = this.MyProvider.GetCredentials()
                };
                if (auth.CredentialStore != null)
                {
                    this.ctx = new TwitterContext(auth);
                    return;
                }
                throw new Exception("Porblem to set Context");
            }
       
        }
        #endregion

        public async Task StartAgainAsync()
        {
            _logger.Info("Ponownie rozpoczynam Prace" + MySearcher.Name);
            if ((SinceID == 0) && (MaxID == 0))  //trzeba przetestować jak to sięzachowa asnychronicznie ( loock, lub sprawdzać sinceId (prywatne downloadea)
                await ManageWorkAsyc();
            if ((SinceID != 0) && (MaxID == 0))  //trzeba przetestować jak to sięzachowa asnychronicznie ( loock, lub sprawdzać sinceId (prywatne downloadea)
                DownloadCycleAsync();
            await DownloadTweetByRangeAsync(); //z odpowiednimy parametrami
        }

        /// <summary>
        /// Metoda rozdziela prace i inicjalizuje pobieranie 
        /// </summary>
        public async Task ManageWorkAsyc()
        {
            List<Downloader> lstD = new List<Downloader>();
            List<Task> lstDTask = new List<Task>();

            ulong startID; //określa ID Tweeta od którego rozpoczyna się pobieranie ( wącznie)
            ulong stopID; //określna górny zakres pobieranych tweetów ID (bez)

            //zeby pracować musi mieć Providera
            if (MyProvider == null)  //nie jest null jeśli jest odpoalana jeszcze raz ( ma nadanie)
                MyProvider = ManageProviders.Instance.GetProviderForManage();
            if (MyProvider == null)
            {
                _logger.Info("ManageWorkAsyc: Dodaje Downloader do listy subskrybętow-" + MySearcher.Name);
                ManageProviders.Instance.Subscribe(this); //dodajemy do listy subskrybędtow i czeka na providera 
                return;
            }

            CheckStartDate();
            //Usatalam najstarszą datę 
            //Zmiana daty
            var Status_Date = await LastTweetID_StartDateAsync(MySearcher.Start.AddDays(-1));
            if (Status_Date.Count() != 1)
                throw new Exception("Problem to get Start dateTime");
            DateTime currentStartDate = Status_Date.FirstOrDefault().Value.AddDays(1);
            MaxDay = Math.Abs((DateTime.Today - currentStartDate).TotalDays);
            MaxDay = MaxDay < 1 ? 1 : MaxDay;
            MySearcher.Start = currentStartDate;
            MySearcher.DownloadCounter = Convert.ToInt16(MaxDay);
            startID = Status_Date.FirstOrDefault().Key;

            for (int i = 0; i <= MaxDay; i++) //MaxDay
            {
                //do najbliższego znależionego
                stopID = await LastTweetIDByDateAsync(MySearcher.Start.AddDays(i)); //daj ostatni Tweet z następnego dnia ( górna zapora MaxID) 

                lstDTask.Add(new Downloader(MySearcher, startID, stopID).DownloadTweetByRangeAsync());
                startID = stopID;

            }




            //foreach (var down in lstD)
            //{
            //    //Task.Factory.StartNew(() => down.DownloadTweetByRangeAsync());
            //    await down.DownloadTweetByRangeAsync();
            //}
            MyProvider.EndWork();
            Task.WaitAll(lstDTask.ToArray());

            StartDownloadCycle(MySearcher);
        }

        private void StartDownloadCycle(Searcher mySearcher)
        {
            if (MySearcher.DownloadCounter == 0)
            {
                ManageCycleDownload.Instance.Add(MySearcher);
            }
        }

        /// <summary>
        /// Sprawdzam czy data startu nie przekroczyła ustalonego limitu 7 dni
        /// jeśli tak-zamieniam
        /// </summary>
        private void CheckStartDate()
        {
            if ((DateTime.Today - MySearcher.Start.Date).TotalDays >= 7)
            {
                MySearcher.Start = DateTime.Today.AddDays(-7);
            }
        }

        private async Task<ulong> LastTweetIDByDateAsync(DateTime dateTime)
        {
            try
            {
                var searchResponse =
                      (from search in ctx.Search
                       where search.Type == SearchType.Search &&
                             search.Query == MySearcher.Query &&
                             search.Count == tweetCount
                             && search.Until == dateTime
                       select search.Statuses).FirstOrDefault();
                if (searchResponse.Any())
                {
                    return searchResponse.First().StatusID;
                }
                //DateTime td = new DateTime(2017, 1, 14);

                //var searchResponse2 =
                //        (from search in ctx.Search
                //         where search.Type == SearchType.Search &&

                //               search.Query == MySearcher.Query &&
                //               search.Count == 100 
                //               &   
                //               search.Until == td                          
                //         select search.Statuses);

                //var tr = searchResponse2.ToList();


                throw new Exception("SLastTweetIDByDateAsync: earch response nie pobrał danych");
            }
            catch (Exception ex)
            {
                _logger.Error("Problem to set LastID By Date " + dateTime);
                _logger.Error(ex.Message);
                throw ex;
            }
        }


        private async Task<Dictionary<ulong, DateTime>> LastTweetID_StartDateAsync(DateTime dateTime)
        {
            bool isCorect = false;
            ulong statusId = 0;
            do
            {
                try
                {
                    statusId = await LastTweetIDByDateAsync(dateTime);

                }
                catch (Exception ex)
                {
                    isCorect = false;
                    dateTime = dateTime.AddDays(1);
                }
                if (statusId != 0)
                {
                    return new Dictionary<ulong, DateTime>() { { statusId, dateTime } };
                    isCorect = true;

                }
            }
            while (isCorect);

            throw new Exception("SLastTweetIDByDateAsync: earch response nie pobrał danych");

        }

        //faktycznie pobieranie danych 
        internal async Task DownloadTweetByRangeAsync()
        {

            if (this.SinceID == this.MaxID)
            {
                _logger.Error("Bad since and Max ID");
                return;
            }
            bool EndWork = false;

            while (!EndWork) //rob cały czas do puki Ci nie przere. End Work
            {
                if (MyProvider == null) //może być przypisany Mianowany jeśli to jest ponowne ruszanie
                    MyProvider = ManageProviders.Instance.GetProvider();
                if (MyProvider == null) //nie ma porvidera 
                {
                    _logger.Info("DownloadTweetByRangeAsync: Dodaje Downloader do listy subskrybetow-" + MySearcher.Name);
                    ManageProviders.Instance.Subscribe(this); //dodajemy do listy subskrybędtow i czeka na providera
                    return;
                }
                EndWork = await ProviderDownloadTwAsync(); //pobiera tweety z zadanego zakresu a w razie błędu wyrzuc bład, a jak skończysz zeróc true 
            }
            if (EndWork)
            {
                ManageProviders.Instance.DeleteDownloader(this);
                MySearcher.DownloadCounter -= 1;
                StartDownloadCycle(MySearcher);
            }
        }

        /// <summary>
        /// Pobiera tweety przy uzyciu okreslonego porvidera ( nie może se go pobrać) a w razie bledu sprawdza czy to limit i blokuje providera
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ProviderDownloadTwAsync()
        {
            var searchResponse = new List<Status>();
            do
            {
                try
                {
                    searchResponse = await DownloadTweetAsync();
                    var t = searchResponse.Count();
                   // throw new Exception();
                }
                catch (Exception ex)
                {
                    if (searchResponse.Any())
                        this.MaxID = searchResponse.FirstOrDefault().StatusID - 1;

                    if (MyProvider.CheckLimitSafe()) //bez ! TRUE JESŁI LIMIT
                    {
                        ManageProviders.Instance.ProviderIsBlocked(MyProvider);  //wywałił błąd bo prawedopodonie jest limit 
                        MyProvider = null;
                    }
                    else
                   {
                        _logger.Error("Nieokreślony typ blokady Providera " + ex.Message);
                        _logger.Error(ex.StackTrace);
                   }
                    return false;
                }

                if (searchResponse.Any())
                {
                    this.MaxID = searchResponse.LastOrDefault().StatusID - 1;//pobierma dane asnychronicznie ( procesor nie czeka na nie tylko robi resze 
                                                                             //reszte to znaczy wszystko poza tą metodą 
                    var ListTweet = ConvwertStatus2Tweet(searchResponse);
                    SaveInDBAsync(ListTweet);
                }
            } while (searchResponse.Any());

            //MySearcher.DownloadCounter -= 1;
            //if (MySearcher.DownloadCounter == 0)           
            //    ManageCycleDownload.Instance.Add(MySearcher);
                        
            _logger.Info("Downloader finish download tweet");
            return true;
        }


        /// <summary>
        /// tylko pobiera tweety i rzuca exception gdy limit
        /// </summary>
        /// <returns></returns>
        private async Task<List<Status>> DownloadTweetAsync()
        {
            if (IsLocationSet())
            {


                return await
                   (from search in ctx.Search
                    where search.Type == SearchType.Search &&
                             search.Query == MySearcher.Query &&
                                  search.Count == MaxSearchEntriesToReturn &&
                                  search.MaxID == this.MaxID &&
                                  search.SinceID == this.SinceID &&
                                  search.GeoCode == string.Format("{0},{1},{2}km", MySearcher.Latitude, MySearcher.Longitude, MySearcher.Distance)
                    select search.Statuses).FirstOrDefaultAsync();


            }
            else


                return await
                    (from search in ctx.Search
                     where search.Type == SearchType.Search &&
                           search.Query == MySearcher.Query &&
                           search.Count == MaxSearchEntriesToReturn &&
                           search.MaxID == this.MaxID &&
                           search.SinceID == this.SinceID
                     select search.Statuses).FirstOrDefaultAsync();




        }




        private bool IsLocationSet()
        {
            return MySearcher.Distance.HasValue
                && MySearcher.Latitude.HasValue
                && MySearcher.Longitude.HasValue;
        }

        //private async Task<bool> DownloadTweetsAsync()
        //{
        //    var searchResponse = new List<Status>();
        //    do
        //    {

        //        try
        //        {
        //            searchResponse =
        //            (from search in ctx.Search
        //             where search.Type == SearchType.Search &&
        //                   search.Query == "Nato" &&
        //                   search.Count == MaxSearchEntriesToReturn && //59.918376527520316, 10.710928093749999
        //                   search.GeoCode == "42.69328710086295,23.3232327,50km"   //42.69328710086295, 23.3232327
        //                                                                           //  search.MaxID == this.MaxID &&
        //                                                                           //    search.SinceID == this.SinceID
        //             select search.Statuses)
        //           .FirstOrDefault();
        //            var t = searchResponse.Count();
        //        }
        //        catch
        //        {
        //            if (searchResponse.Any())
        //                this.MaxID = searchResponse.FirstOrDefault().StatusID - 1;
        //            MyProvider.CheckLimit();
        //            ProviderIsBloked(); //wywałił błąd bo prawedopodonie jest limit 
        //            return false;
        //        }
        //        if (searchResponse.Any())
        //        {
        //            this.MaxID = searchResponse.LastOrDefault().StatusID - 1;//pobierma dane asnychronicznie ( procesor nie czeka na nie tylko robi resze 
        //                                                                     //reszte to znaczy wszystko poza tą metodą 
        //            var ListTweet = ConvwertStatus2Tweet(searchResponse);

        //            //jak dostaniemy dane to asynchronicznie zapisujemy do bazy
        //            //tu nie czeka ( zrób i zapomnij)
        //            //tz
        //            SaveInDBAsync(ListTweet);
        //        }
        //    } while (searchResponse.Any());

        //    _logger.Info("Downloader finish download tweet");

        //    return true;
        //}

        private List<Tweet> ConvwertStatus2Tweet(List<Status> searchResponse)
        {
            List<Tweet> lstTweet = new List<Tweet>();
            foreach (var s in searchResponse)
            {
                lstTweet.Add(new Tweet()
                {
                    TweeterID = s.StatusID,
                    Date = s.CreatedAt,
                    Text = s.Text,
                    Author = s.User.ScreenNameResponse + " " + s.User.UserIDResponse,
                    SearcherID = MySearcher.Id,
                });
            }
            return lstTweet;
        }

        private void SaveInDBAsync(List<Tweet> searchResponse)
        {
            tweetRepo.AddRangeAsync(searchResponse);
        }



        #region Cycle
        public async Task<bool> DownloadCycleAsync()
        {
            bool EndWork = false;
            this.SinceID = tweetRepo.FindMaxBySearcherID(MySearcher.Id);

            while (!EndWork) //rub cały czas do puki Ci nie przere. End Work
            {
                if (MyProvider == null)  //nie jest null jeśli jest odpoalana jeszcze raz ( ma nadanie)
                    MyProvider = ManageProviders.Instance.GetProvider();
                if (MyProvider == null)
                {
                    _logger.Info("DownloadCycleAsyc: Dodaje Downloader do listy subskrybętow-" + MySearcher.Name);
                    ManageProviders.Instance.Subscribe(this); //dodajemy do listy subskrybędtow i czeka na providera 
                    return true;
                }
                EndWork = await StartDownloadTwCycleAsync();
            }
            MyProvider.EndWork();
            return EndWork;
        }

        /// <summary>
        /// Metoda pobiera dane dla trybu cyklicznego, gdy błąd -Provider ISBlocked
        /// Gdy skączy to kończy
        /// </summary>
        /// <returns></returns>
        private async Task<bool> StartDownloadTwCycleAsync()
        {
            var searchResponse = new List<Status>();
            do
            {
                try
                {
                    searchResponse = await DownloadTweetCycleAsync();
                    var t = searchResponse.Count();
                }
                catch (Exception ex)
                {
                    if (searchResponse.Any())
                        this.MaxID = searchResponse.FirstOrDefault().StatusID - 1;
                    if (MyProvider.CheckLimitSafe())
                    {
                       ManageProviders.Instance.ProviderIsBlocked(MyProvider);  //wywałił błąd bo prawedopodonie jest limit 
                        MyProvider = null;
                    }
                    else
                    {
                        _logger.Error("Nieokreślony typ blokady Providera " + ex.Message);
                        _logger.Error(ex.StackTrace);
                    }
                    return false;
                }
                if (searchResponse.Any())
                {
                    this.SinceID = searchResponse.FirstOrDefault().StatusID; //pobierma dane asnychronicznie ( procesor nie czeka na nie tylko robi resze 
                                                                             //reszte to znaczy wszystko poza tą metodą 
                    var ListTweet = ConvwertStatus2Tweet(searchResponse);
                    SaveInDBAsync(ListTweet);
                }
            } while (searchResponse.Any());
            _logger.Info("Downloader finish download tweet");
            return true;
        }

        /// <summary>
        /// Faktyczne pobieranie danych (to do dynamic query
        /// </summary>
        /// <returns></returns>
        private async Task<List<Status>> DownloadTweetCycleAsync()
        {
            if (!IsLocationSet())
            {
                return await
                   (from search in ctx.Search
                    where search.Type == SearchType.Search &&
                             search.Query == MySearcher.Query &&
                                  search.Count == MaxSearchEntriesToReturn &&
                                  search.SinceID == this.SinceID &&
                                  search.GeoCode == string.Format("{0},{1},{2}km", MySearcher.Latitude, MySearcher.Longitude, MySearcher.Distance)   //42.69328710086295,23.3232327,50km"   //42.69328710086295, 23.3232327
                    select search.Statuses)
                   .FirstOrDefaultAsync();

            }
            else
                return await
                    (from search in ctx.Search
                     where search.Type == SearchType.Search &&
                           search.Query == MySearcher.Query &&
                           search.Count == MaxSearchEntriesToReturn &&
                           search.SinceID == this.SinceID
                     select search.Statuses)
               .FirstOrDefaultAsync();
        }
        #endregion



        #region Observer
        /// <summary>
        /// Metoda wykonywana po inicjalizacji z pozomu Obserwowanego ( gdy zwolni się provider
        /// </summary>
        /// <param name="value"></param>
        public void OnNext(Provider value)
        {
            MyProvider = value;
            StartAgainAsync();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
        #endregion Observer

    }
}