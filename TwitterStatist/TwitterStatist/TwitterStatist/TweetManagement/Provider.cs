﻿using LinqToTwitter;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;


namespace TwitterStatist
{
    public class Provider
    {
        private ILog _logger;
        private TwitterContext ctx;

        private ReaderWriterLockSlim ProviderLock = new ReaderWriterLockSlim();
        private object syncLook = new Object();
        //   public string Name { get; set; }
        /// <summary>
        /// 1-ready,
        /// 2-work,
        /// 3-block
        /// </summary>
        public Flags Flag { get; set; }
        public string TokenAccess { get; set; }
        public string SecretTokenAccess { get; set; }
        public DateTime LastUse { get; set; }
        public int UseCounter { get; set; }

        //public string ConsumerKey
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["consumerKey"];
        //    }
        //    set
        //    {

        //    }
        //}

        //public string ConsumerSecret
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["consumerSecret"];
        //    }
        //    set
        //    {

        //    }
        //}

        //public string OAuthToken
        //{
        //    get
        //    {
        //        return TokenAccess;
        //    }

        //    set
        //    {
        //        TokenAccess = value;
        //    }
        //}

        //public string OAuthTokenSecret
        //{
        //    get
        //    {
        //        return SecretTokenAccess;
        //    }
        //    set
        //    {
        //        SecretTokenAccess = value;
        //    }
        //}

        //public string ScreenName
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }

        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        //public ulong UserID
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }

        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}


        public Provider()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        /// <summary>
        /// zwraca prawdę jesli limit zostal osiągnięty 
        /// </summary>
        /// <returns></returns>
        internal bool CheckLimit()
        {
            try
            {
                var tmpctx = new TwitterContext(DoSingleUserAuth());

                var helpResult =
                   (from help in tmpctx.Help
                    where help.Type == HelpType.RateLimits &&
                                                           help.Resources == "search"
                    select help)
                   .FirstOrDefault();
                var remaing = helpResult.RateLimits["search"].FirstOrDefault().Remaining;
                //jeśli 0 tzn że nic nie zostało i limit jest osiągniety
                return remaing == 0;
            }
            catch (Exception ex)
            {
                _logger.Error("Problem to check Limit");
                return false;
            }
        }

        internal bool CheckLimitSafe()
        {
            ProviderLock.EnterWriteLock();//Blokuje aż do odwołania 
            try
            {
                return CheckLimit();
            }
            finally
            {
                ProviderLock.ExitWriteLock();
            }
        }
        IAuthorizer DoSingleUserAuth()
        {
            //   MyProvider.OAuthToken = "";
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = this.GetCredentials()
            };
            return auth;
        }


        //internal void ChangeFlags(Flags flag)
        //{
        //   bool wrongOperation = false;
        //    switch (flag)
        //    {
        //        case Flags.Ready:
        //            if ((Flag ==(int)Flags.Block)|| (Flag == (int)Flags.Work) ) //jeśli była work lub Block
        //                SetFlag(Flags.Ready);
        //            else
        //                wrongOperation = true;
        //            break;
        //        case Flags.Block:
        //            if (Flag == (int)Flags.Work)
        //                SetFlag(Flags.Work);
        //            else
        //                wrongOperation = true;
        //            break;
        //        case Flags.Work:
        //            if ((Flag == (int)Flags.Block) || (Flag == (int)Flags.Ready))
        //                SetFlag(Flags.Work);
        //            else
        //                wrongOperation = true;
        //            break;
        //        default:
        //            break;
        //    }
        //    if (wrongOperation) ;
        //        //Logger(Wrong change Flags {0}, {1});
        //}



        /// <summary>
        /// Blokuje providera i zmiania mu flagę na Work 
        ///  oraz zwiększa CounterWork
        /// </summary>
        /// <returns></returns>
        internal Provider PrepereToUse()
        {
            ProviderLock.EnterWriteLock();
            try
            {
                this.Flag = Flags.Work;
                this.UseCounter += 1;
                return this;
            }
            finally
            {
                _logger.Info("Dopasowano prowi");

                ProviderLock.ExitWriteLock();
            }
        }

        internal void BlockMe()
        {
            ProviderLock.EnterWriteLock();

            try
            {
                if (Flag != Flags.Block) //jeśli flaga jest taka sama return true 
                {                                        //jesli jeszcze zaden process mnie nie zablokował 
                    Flag = Flags.Block;
                    UseCounter = 0;
                    IntrvalCheckLimit();
                }
            }
            finally
            {
                ProviderLock.ExitWriteLock(); //opuszczam liste
                _logger.Info("porvider zablokowany");
                //uruchamiam mechaniz oczekiwania na zmiane Limitu
            }
        }


        public SingleUserInMemoryCredentialStore GetCredentials()
        {
            return new SingleUserInMemoryCredentialStore
            {
                ConsumerKey = ConfigurationManager.AppSettings["consumerKey"],
                ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"],
                AccessToken = TokenAccess,
                AccessTokenSecret = SecretTokenAccess
            };
        }
        internal Provider ChangeFlag(Flags flags)
        {
            ProviderLock.EnterWriteLock();
            try
            {
                this.Flag = flags;
                return this;
            }
            finally
            {
                ProviderLock.ExitWriteLock();
            }
        }

        internal void IsReady()
        {
            //ProviderLock.EnterWriteLock();
            //try
            //{
            Flag = Flags.Ready;
            UseCounter = 0;
            //}
            //finally
            //{
            //    ProviderLock.ExitWriteLock();
            //}
        }

        //private void SetFlag(Flags flag)
        //{ // co w przypadku gdy nie chce juz nic asynchronicznie robić 
        //  if ( Flag!=(int)flag)
        //    {
        //        lock(syncLook)
        //        {
        //            Flag = (int)flag;
        //            Console.WriteLine("Provider " + Name + " change flag to "+ flag);
        //        }
        //    }
        //}



        public void OnNext(Provider value)
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sprawdza czy flaga jest juz ustwiony, look tylko na Read
        /// </summary>
        /// <param name="flags"></param>
        /// <returns></returns>
        internal bool CheckFlag(Flags flags)
        {
            ProviderLock.EnterReadLock();
            try
            {
                return this.Flag == flags;

            }
            finally
            {
                ProviderLock.ExitReadLock();
            }
        }
        //  private bool IsLimit { get; set; }
        private System.Timers.Timer oTimer = new System.Timers.Timer();

        public void IntrvalCheckLimit()
        {
            oTimer.Elapsed += new ElapsedEventHandler(CheckLimitForProvider);
            oTimer.Interval = 60000 * 11; //2 min
            oTimer.Enabled = true;
        }

        private async void CheckLimitForProvider(object oSource, ElapsedEventArgs oElapsedEventArgs)
        {
            oTimer.Interval = 60000;
            ProviderLock.EnterWriteLock();//Blokuje aż do odwołania 
            try
            {                                        //true jeśli juz nie ma limitu
                if (Flag == Flags.Block)
                    if (!CheckLimit()) //Limit ?//metoda sprawdzajaca limit dla danego providera AND FLAG
                    {
                        try
                        {
                            IsReady();
                            oTimer.Stop();
                            await ManageProviders.Instance.LetKnowDownloaders();
                            _logger.Info("Porvider nie ma już limitu");

                        }
                        catch (Exception ex)
                        {
                        }
                    }

            }
            finally
            {
                ProviderLock.ExitWriteLock();
            }
        }

        public bool HasAllCredentials()
        {
            throw new NotImplementedException();
        }

        public Task LoadAsync()
        {
            throw new NotImplementedException();
        }

        public Task StoreAsync()
        {
            throw new NotImplementedException();
        }

        public Task ClearAsync()
        {
            throw new NotImplementedException();
        }

        internal void EndWork()
        {
            ProviderLock.EnterWriteLock();//Blokuje aż do odwołania 
            try
            {
                UseCounter -= 1;
                if (UseCounter == 0 && Flag != Flags.Ready)
                    Flag = Flags.Ready;
            }
            finally
            {
                ProviderLock.ExitWriteLock();
            }
        }
    }




    public enum Flags { Ready = 1, Work, Block };

}

