﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Threading;
using log4net;
using TwitterStatist.Repositores;
using System.Reflection;
using System.Timers;

namespace TwitterStatist
{

    public sealed class ManageProviders : IObservable<Provider>
    {
        private ILog _logger;
        private UserRepository _userRepo;
        private UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }

        private static volatile ManageProviders instance;

        private static object syncLook = new Object();  //Singleton dla ManageProviders
                                                        //     private static object listLook = new Object();
        private int DeftUseCounter = 3; //liczba downloaderów na jednego providera
        private static object checkinglook = new Object();
        private ReaderWriterLockSlim ProvidersLock = new ReaderWriterLockSlim(); //lock na liste Providerów 
        static IList<Provider> Providers { get; set; } // Lista providerów ( wszystkich jakie są w bd)
                                                       //   static IList<Provider> WorkProviders { get; set; }
        private IList<IObserver<Provider>> Downloaders { get; set; } //dla iobservable ( Lista obserwujących Downloaderów ( reagują na Providera)


        private ManageProviders()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            Downloaders = new List<IObserver<Provider>>();

            var accessTokens = userRepo.GetAllAccessToken();
            Providers = new List<Provider>();

            foreach (var at in accessTokens)
            {
                Providers.Add(
                            new Provider
                            {
                                SecretTokenAccess = at.TokenAccess,
                                TokenAccess = at.SecretTokenAccess,
                                LastUse = DateTime.Now,
                                Flag = Flags.Ready,
                                UseCounter = 0
                            });
            }
            _logger.Info("Create Providers, count= " + Providers.Count());
        }

        //  private IList<IObserver<Provider>> DownloadersManage { get; set; } //dla iobservable
        /// <summary>
        /// Metoda dostarcza providera według rygorystycznych zasad
        /// Zwraca Providera jeśli jest Ready lub Work i licznik użycia nie jest większy niż 3
        /// w innym przypadku zwraca null, co oznacza że należy poczekać na providera.
        /// </summary>
        /// <returns></returns>
        internal Provider GetProvider()
        {
            Provider pr = null;
            ProvidersLock.EnterReadLock(); //read , jesli znajdzie sie ready to zmieniamy, jeśłi work to nie zmieniamy
            try
            {
                pr = Providers.Where(p => p.Flag == Flags.Ready).FirstOrDefault();
                if (pr == null)
                {
                    pr = Providers.Where(p => ((p.Flag == Flags.Work) && (p.UseCounter <= DeftUseCounter))).FirstOrDefault(); //workMode

                }
                if (pr != null)
                {

                    return pr.PrepereToUse();// brak syncronicznośći bo i tak czekamy na powrót 

                }
                else
                {
                    return null;
                }
            }
            finally
            {
                ProvidersLock.ExitReadLock();
            }
        }



        /// <summary>
        /// Daj provadera który wszystkich tylko nie bloked jak nie to czekaj, nie zwracaj null
        /// </summary>
        /// <returns></returns>
        internal Provider GetProviderForManage()
        {
            Provider pr = null;
            ProvidersLock.EnterReadLock(); //read , jesli znajdzie sie ready to zmieniamy, jeśłi work to nie zmieniamy
            try
            {
                pr = Providers.Where(p => p.Flag == Flags.Ready).FirstOrDefault();
                if (pr == null)
                {
                    pr = Providers.Where(p => p.Flag == Flags.Work).FirstOrDefault(); //Bez znaczenia na UseCounter

                }
                if (pr != null)
                {

                    return pr.PrepereToUse();// brak syncronicznośći bo i tak czekamy na powrót 
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                ProvidersLock.ExitReadLock();
            }
        }

        public void ProviderIsBlocked(Provider myProvider)
        {
            myProvider.BlockMe();
        }

        /// <summary>
        /// odbiera informacje że dany provder jest już dostępny 
        /// </summary>        
        internal async Task ProviderIsReady(Provider provider)
        {
          //  provider.IsReady();
            await LetKnowDownloaders();
        }

        public async Task LetKnowDownloaders()
        {
            ProvidersLock.EnterReadLock();
            try
            {
                if (Downloaders.Any())
                {
                    foreach (Downloader downloader in Downloaders.Take(DeftUseCounter))
                    {
                        await downloader.StartAgainAsync();
                    }
                }
            }
            finally
            {
                ProvidersLock.ExitReadLock();
            }
        }

        public static ManageProviders Instance
        {
            get
            {

                if (instance == null)
                {
                    lock (syncLook)
                    {
                        if (instance == null)
                        {
                            instance = new ManageProviders();
                        }
                    }
                }
                return instance;
            }
        }

        public IDisposable Subscribe(IObserver<Provider> downloader)
        {
            if (!Downloaders.Contains(downloader))
                Downloaders.Add(downloader);
            //  StartChecking();
            return null;
        }

        internal void DeleteDownloader(Downloader downloader)
        {
            if (Downloaders.Contains(downloader))
                Downloaders.Remove(downloader);
        }
    }
}

