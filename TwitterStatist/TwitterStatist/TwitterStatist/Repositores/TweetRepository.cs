﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TwitterStatist.Models;

namespace TwitterStatist.Repositores
{
    public class TweetRepository : ITweetRepository
    {
        private UserRepository _userRepo;
        public UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }

        private readonly TwitterStatistContext _db;

        public TweetRepository()
        {
            _db = new TwitterStatistContext();
        }
        void ITweetRepository.Add(Tweet tweet, string searchId)
        {
            //ApplicationUser user = userRepo.GetById(searchId);
            //user.Tw.Add(tweet);
        }
        public void Add(Tweet tweet)
        {
            try
            {
                //_db.Tweets.Add(tweet);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }

        //public void AddAsync(Tweet Tweet)
        //{
        //    _db.Tweets.AddAs

        //}

        public void AddRange(List<Tweet> tweetList)
        {
            //_db.Tweets.AddRange(tweetList);
            _db.SaveChanges();
        }
        public void AddRangeAsync(List<Tweet> tweetList)
        {
            _db.Tweets.AddRange(tweetList);
            _db.SaveChanges();
        }

        public void Add(Tweet tweet, string searchId)
        {
            throw new NotImplementedException();
        }

        public Tweet GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Save(Tweet entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tweet entity)
        {
            throw new NotImplementedException();
        }
        
        IEnumerable<Tweet> ITweetRepository.FindById(int Id)
        {
            throw new NotImplementedException();
        }

        internal ulong FindMaxBySearcherID(int searcherId)
        {
            
              var et=  _db.Tweets.Where(t => t.SearcherID == searcherId).Max(t => t.StatusID);

            return (ulong)et;
        }
    }
}
