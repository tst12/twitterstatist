﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TwitterStatist.Models;
using System.Data.Entity;
using log4net;
using System.Reflection;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using AutoMapper;

namespace TwitterStatist.Repositores
{
    public class UserRepository : IUserRepository
    {
        private readonly TwitterStatistContext _db;
        private ILog _logger;

        public UserRepository()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _db = new TwitterStatistContext();
        }

        public IQueryable<ApplicationUser> GetAllUsers()
        {
            return _db.Users;
        }

        public void Add(ApplicationUser user)
        {
            _db.Users.Add(user);
        }

        public ApplicationUser Create()
        {
            ApplicationUser user = new ApplicationUser();

            return user;
        }

        public List<AccessToken> GetAllAccessToken()
        {
            return _db.AccessTokens.ToList();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public IEnumerable<ApplicationUser> FindAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApplicationUser> Find(string text)
        {
            throw new NotImplementedException();
        }

        public ApplicationUser GetById(string id)
        {
            return _db.Users.Where(x => x.Id == id).Single();
        }


        public bool HaveAdimRole(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_db));
                var s = UserManager.GetRoles(id);
                return s.Where(r => r.Contains("Admin")).Any();
            }
            return false;

        }


        /// <summary>
        /// Dodawanie Searchera 
        /// </summary>
        /// <param name="search"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddSearcher(Searcher search, string userId)
        {
            var user = GetById(userId); 
            user.Searchers.Add(search);
            _db.Entry(user).State = EntityState.Modified;
            _db.SaveChanges();
            return search.Id;
        }



        public void Save(ApplicationUser entity)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Metoda sprawdza czy Token przypisany do usera jset taki sam jak aktualnie uzyskany 
        /// jeśli jest inny to update na nim
        /// </summary>
        /// <param name="acT"></param>
        /// <param name="userId"></param>
        public void UpdateAccessToken(AccessToken acT, string userId)
        {
            var user = GetById(userId);
            var uAT = user.AccessToken;
            if (uAT != null)
            {
                if (!(uAT.TokenAccess.Equals(acT.TokenAccess) && uAT.SecretTokenAccess.Equals(acT.SecretTokenAccess)))
                {
                    //_logger.Info("Update accessToken for user");                  
                    uAT.TokenAccess = acT.TokenAccess;
                    uAT.SecretTokenAccess = acT.SecretTokenAccess;
                    _db.Entry(uAT).State = EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            else
            {
                user.AccessToken = acT;
                _db.Entry(user).State = EntityState.Modified;
                _db.SaveChanges();
            }
        }

        public void Delete(ApplicationUser entity)
        {
            throw new NotImplementedException();
        }

        public UserInfoViewModel GetUserInfoVModel(ApplicationUser user)
        {
            return UserToUserInfoVModel(user);
        }

  
        private List<UserViewModel> UserListToModelView(List<ApplicationUser> users)
        {
            //   Mapper.CreateMap<Searcher, SearcherViewModel>();
            var mapr = Mapper.Map<List<ApplicationUser>, List<UserViewModel>>(users);
            return mapr;
        }
        private List<UserInfoViewModel> UserListToInfoMView(List<ApplicationUser> users)
        {
            List<UserInfoViewModel> results = new List<UserInfoViewModel>();

            foreach (var user in users)
            {
                results.Add(UserToUserInfoVModel(user));
            }
            return results;
            //   Mapper.CreateMap<Searcher, SearcherViewModel>();
  //          var mapr = Mapper.Map<List<ApplicationUser>, List<UserInfoViewModel>>(users);
    //        return mapr;
        }

        private UserInfoViewModel UserToUserInfoVModel(ApplicationUser user)
        {
            var mapr = Mapper.Map<ApplicationUser,UserInfoViewModel>(user);
            mapr.SearcherCount = GetSearchCount(user);
            return mapr;
        }

        private int GetSearchCount(ApplicationUser user)
        {
            return user.Searchers.Count();
        }

        internal List<UserViewModel> GetAllUsersVModel()
        {
             var   searchList = UserListToModelView(_db.Users.ToList());
            return searchList;
        }
        internal List<UserInfoViewModel> GetAllUsersInfoVModel()
        {
            var searchList = UserListToInfoMView(_db.Users.ToList());
            return searchList;
        }
        internal SearcherViewModel GetSearcherVModel(int? id)
        {
            var search = _db.Searchers.FindAsync(id).Result;
            return SearcherToModelView(search);
        }

     
        public SearcherViewModel SearcherToModelView(Searcher searcher)
        {
            //   Mapper.CreateMap<Searcher, SearcherViewModel>();
            var mapr = Mapper.Map<Searcher, SearcherViewModel>(searcher);
            return mapr;
        }

        internal void SetBlock(string id, bool isBlock)
        {
            var user=GetById(id);
            user.IsBlocked = isBlock;
           // SaveChanges();

            _db.Entry(user).State = EntityState.Modified;
            _db.SaveChanges();

        }
    }
}