﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterStatist.Models;
using TwitterStatist.Repositores;

namespace TwitterStatist.Repositores
{
    interface ISearcherRepository : IRepository<Searcher, int>
    {
        IEnumerable<Searcher> FindAllByUserId(string userId);

        IEnumerable<Searcher> FindById(int Id);

         void Add(Searcher search);
        void Add(Searcher search, string userId);
    }
}
