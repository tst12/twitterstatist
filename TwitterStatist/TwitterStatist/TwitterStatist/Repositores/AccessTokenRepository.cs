﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TwitterStatist.Models;

namespace TwitterStatist.Repositores
{
    public class AccessTokenRepository
    {


        private TwitterStatistContext _db;
       
        public AccessTokenRepository()
        {
            _db = new TwitterStatistContext();
        }

        public IQueryable<AccessToken> GetAllAccessTokens()
        {
            return _db.AccessTokens;
        }

        public void Add (AccessToken accsessToken)
        {
            _db.AccessTokens.Add(accsessToken);
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}