﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterStatist.Models;

namespace TwitterStatist.Repositores
{
    interface ITweetRepository : IRepository<Tweet,int>
    {
        IEnumerable<Tweet> FindById(int Id);
        void Add(Tweet tweet);
        void Add(Tweet tweet, string searchId);
    }
}
