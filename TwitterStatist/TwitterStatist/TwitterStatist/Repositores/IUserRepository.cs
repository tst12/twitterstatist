﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterStatist.Models;

namespace TwitterStatist.Repositores
{
    interface IUserRepository : IRepository<ApplicationUser, string>
    {

        IEnumerable<ApplicationUser> FindAll();
        IEnumerable<ApplicationUser> Find(string text);

    }
}
