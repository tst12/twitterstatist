﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TwitterStatist.Models;
using AutoMapper;
namespace TwitterStatist.Repositores
{


    public class SearcherRepository : ISearcherRepository
    {
        ILog _logger;
        private UserRepository _userRepo;
        public UserRepository userRepo
        {
            get
            {
                if (_userRepo == null)
                    _userRepo = new UserRepository();
                return _userRepo;
            }
            set
            {
                _userRepo = value;
            }
        }

        private readonly TwitterStatistContext _db;

        public SearcherRepository()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _db = new TwitterStatistContext();
        }
        void ISearcherRepository.Add(Searcher search, string userId)
        {
            ApplicationUser user = userRepo.GetById(userId);
            user.Searchers.Add(search);
        }

        void IRepository<Searcher, int>.Delete(Searcher entity)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Searcher> ISearcherRepository.FindAllByUserId(string userId)
        {
            ApplicationUser user = userRepo.GetById(userId);
            return user.Searchers;
        }

        IEnumerable<Searcher> ISearcherRepository.FindById(int Id)
        {
            throw new NotImplementedException();
        }

        internal List<SearcherViewModel> GetAllSearchVMByUser(string userId)
        {
            List<SearcherViewModel> searchList = new List<SearcherViewModel>();            
            var user = _db.Users.Where(u => u.Id.Equals(userId)).Single();
            if (user!=null)
            {
                searchList = SearchListToModelView(user.Searchers.ToList());
            }
            return searchList;
        }

        internal object GetRandomSearchVM(int randomCount)
        {
            List<SearcherViewModel> searchList = new List<SearcherViewModel>();
            searchList = SearchListToModelView(_db.Searchers.OrderBy(x=>Guid.NewGuid()).Take(randomCount).ToList());            
            return searchList;
        }

        internal SearcherViewModel GetSearcherVModel(int? id)
        {
           var search= _db.Searchers.FindAsync(id).Result;
            return SearcherToModelView(search);
        }

        private List<SearcherViewModel> SearchListToModelView(List<Searcher> searchers)
        {
         //   Mapper.CreateMap<Searcher, SearcherViewModel>();
         var mapr=  Mapper.Map<List<Searcher>, List<SearcherViewModel>>(searchers);
            return mapr;
        }
        public SearcherViewModel SearcherToModelView(Searcher searcher)
        {
            //   Mapper.CreateMap<Searcher, SearcherViewModel>();
            var mapr = Mapper.Map<Searcher,SearcherViewModel>(searcher);
            return mapr;
        }
        public Searcher  ModelViewToSearcher(SearcherViewModel searcherMV)
        {
            //   Mapper.CreateMap<Searcher, SearcherViewModel>();
            var mapr = Mapper.Map<SearcherViewModel,Searcher>(searcherMV);
            return mapr;
        }

        internal List<Tweet> GetTweetBySearcherID(int searcherId)
        {
            var searcher = GetById(searcherId);

            return searcher.Tweets.ToList();   
        }

        Searcher IRepository<Searcher, int>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        void IRepository<Searcher, int>.Save(Searcher entity)
        {
            throw new NotImplementedException();
        }
        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public void Add(Searcher search)
        {
            _db.Searchers.Add(search);
            SaveChanges();
        }
        //public List<Land> GetAllLands()
        //{
        //    return _db.Lands.ToList();
        //}
        //public List<QueryMode> GetAllQueryMode()
        //{
        //    return _db.QueryModes.ToList();
        //}
        public List<WorkMode> GetAllWorkMode()
        {
            return _db.WorkModes.ToList();
        }

        public void AddToUser(Searcher search, string userId)
        {
            search = CompleteNewSearcher(search); //przygotowuje do zapisu
            try
            {
                userRepo.AddSearcher(search, userId); //zapisuje całość w bazie 
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error with created Searcher{0} for user {1}", search.Name, userId);

            }
            finally
            {
                SaveSearcher(search.Id);
                _logger.InfoFormat("Create Searcher{0} for user {1}", search.Name, userId);
            }
        }

        /// <summary>
        /// Metoda uzupełnia nowy searcher o potrzevne informacje 
        /// </summary>
        /// <param name="search"></param>
        private Searcher CompleteNewSearcher(Searcher search)
        {
            search.Start =((DateTime.Today-search.Start).TotalDays>7)? DateTime.Today.AddDays(-7):search.Start;
            search.IdWorkMode = (int)Flags.Ready;

            //    var land = _db.Lands.Where(l => l.ID == search.IdLand).FirstOrDefault();
            //   var workMode = _db.WorkModes.Where(l => l.ID == search.IdWorkMode).FirstOrDefault();
            //  var qwueryMode = _db.QueryModes.Where(l => l.ID == search.IdQueryMode).FirstOrDefault();

            //  search.Land = land;
            //  search.QeryModes = qwueryMode;
            //  search.WorkModes = workMode;
            return search;
        }

        internal void AddSearcherMVToUser(SearcherViewModel searcherMV, string userId)
        {
           var searcher= ModelViewToSearcher(searcherMV);
            searcher = CompleteNewSearcher(searcher); //przygotowuje do zapisu
            try
            {
                userRepo.AddSearcher(searcher, userId); //zapisuje całość w bazie 
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error with created Searcher{0} for user {1}", searcher.Name, userId);
                return;
            }
            finally
            {
                SaveSearcher(searcher.Id);
                _logger.InfoFormat("Create Searcher{0} for user {1}", searcher.Name, userId);
            }
        }

        private void SaveSearcher(int searchId)
        {

            var search = GetById(searchId);
            if (search != null)
            {
                //var land = _db.Lands.Where(l => l.ID == search.IdLand).FirstOrDefault();
                var workMode = _db.WorkModes.Where(l => l.ID == search.IdWorkMode).FirstOrDefault();
                //var qwueryMode = _db.QueryModes.Where(l => l.ID == search.IdQueryMode).FirstOrDefault();

                //search.Land = land;
                //search.QeryModes = qwueryMode;
                search.WorkModes = workMode;

                SaveChanges();
            }
            else
                throw new Exception("Error with add another data to seatcher");
        }


        public Searcher GetById(int searchId)
        {
            return _db.Searchers.Where(s => s.Id == searchId).FirstOrDefault();
        }

        internal Searcher ChengeWModeTo(int searcherId, int wMId)
        {
            var s = GetById(searcherId);
            s.IdWorkMode = wMId;
            SaveChanges();
            return s;
        }
    }
}