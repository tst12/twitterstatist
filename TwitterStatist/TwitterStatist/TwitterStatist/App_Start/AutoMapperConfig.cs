﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Diagnostics;
using TwitterStatist.Models;

namespace TwitterStatist.App_Start
{
    public static class AutoMapperConfig
    {


        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new SearcherViewModelProfile());
                cfg.AddProfile(new ViewModelSearcherProfile());
                cfg.AddProfile(new UserViewModelProfile());
                cfg.AddProfile(new UserInfoViewModelProfile());

            });
        }
    }   
}