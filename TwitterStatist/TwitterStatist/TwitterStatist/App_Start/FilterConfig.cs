﻿using System.Web;
using System.Web.Mvc;
using TwitterStatist.Filters;

namespace TwitterStatist
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
