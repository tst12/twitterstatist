﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwitterStatist.Models
{
    public class AccessToken
    {


        public AccessToken() { }

        public AccessToken(ApplicationUser user)
        {
         //   ApplicationUser = user;
            LastUse = DateTime.Today;
        }

        [Key]
        public int ID { get; set; }
     //   public string UserID { get; set; }
        public string TokenAccess { get; set; }
        public string SecretTokenAccess { get; set; }
        public DateTime LastUse { get; set; }
        public  ApplicationUser ApplicationUser { get; set; }

    }

}