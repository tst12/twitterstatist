﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using LinqToTwitter;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace TwitterStatist.Models
{
    public enum SearchWorkMode
    {
        New = 1,
        Search,
        Follow,
        Stop
    };

    public class Searcher
    {
        public int Id { get; set; }
        [ForeignKey("ApplicationUser")]
        public string IdUser { get; set; }
        public string Name { get; set; }
        public int IdWorkMode { get; set; }
        public string Query { get; set; }
        public DateTime Start { get; set; }
        public WorkMode WorkModes { get; set; }
        public double? Distance { get; set; }       
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public virtual ICollection<Tweet> Tweets { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        [NotMapped]
        public int DownloadCounter { get; set; }

    }


    public class SearcherViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Autor")]
        public string IdUser { get; set; }
        [Display(Name = "Nazwa")]
        [RegularExpression(@"^.{3,}$", ErrorMessage = "wymagane minimum 3 znaki")]
        [Required(ErrorMessage = "pole wymagane!")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Invalid")]
        public string Name { get; set; }


        public int IdWorkMode { get; set; }
        [Display(Name = "Tryb pracy")]
        public SearchWorkMode  WorkMode { get; set; }
        [Display(Name = "Szukana fraza")]
        [RegularExpression(@"^.{3,}$", ErrorMessage = "wymagane minimum 3 znaki")]
        [Required(ErrorMessage = "pole wymagane!")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Invalid")]
        public string Query { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", NullDisplayText =" ",ApplyFormatInEditMode = true)]
        public DateTime Start { get; set; }
        public string Distance { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public bool LocationValue { get; set; }
    }
    class Stat
    {
        public ulong Date;
        public double Value;

        public Stat(ulong d, double v)
        {
            Date = d;
            Value = v;
        }
    }


    #region AutoMap

    public class SearcherViewModelProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Searcher, SearcherViewModel>()
                .ForMember(dest => dest.WorkMode, opt => opt.MapFrom(src => (SearchWorkMode)src.IdWorkMode))
                .ForMember(d => d.Distance, opt => opt.MapFrom(src => src.Distance.ToString()))
                .ForMember(d => d.Latitude, opt => opt.MapFrom(src => src.Latitude.ToString()))
                .ForMember(d => d.Longitude, opt => opt.MapFrom(src => src.Longitude.ToString()));

        }

    }
    public class ViewModelSearcherProfile : Profile
    {
        protected override void Configure()
        {
            //TSource,TDestination
            CreateMap<SearcherViewModel, Searcher>()
                .ForMember(d => d.Latitude, opt => opt.MapFrom(src => ParseToDouble(src.Latitude)))
                .ForMember(d => d.Distance, opt => opt.MapFrom(src => ParseToDouble(src.Distance)))
                .ForMember(d => d.Longitude, opt => opt.MapFrom(src => ParseToDouble(src.Longitude)));

        }

        private double? ParseToDouble(string doubleString)
        {
            double value;
            if (double.TryParse(doubleString, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                return value;
           return null;
        }
    }


    #endregion


}