﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwitterStatist.Models
{
    public class Tweet
    {
        public long ID { get; set; }
        public long StatusID{ get; set; }
        public int SearcherID { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
        public string AuthorID { get; set; }
        public string Lang { get; set; }
        public virtual Searcher Searcher { get; set; }

        [NotMapped]
        public ulong TweeterID
        {
            get
            {
                unchecked
                {
                    return (ulong)StatusID;
                }
            }
            set
            {
                unchecked
                {
                    StatusID = (long)value;
                }
            }

        }
    }
}