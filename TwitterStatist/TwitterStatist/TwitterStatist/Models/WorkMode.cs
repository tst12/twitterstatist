﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwitterStatist.Models
{
    public enum WMode
    {
        New = 1,
        Follow = 2,
        Pause = 3,
        Stop = 4,
        Waiting = 5,
        Search = 6
    }
    public class WorkMode
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public virtual ICollection<Searcher> Searchers { get; set; }

    }
}