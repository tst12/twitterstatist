﻿using System.Collections.Generic;

namespace TwitterStatist.Models
{
    public class Land
    {
        public int ID { get; set; }
        public int Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Tweet> Tweets { get; set; }
        public virtual ICollection <Searcher> Searchers { get; set; }
    }
}