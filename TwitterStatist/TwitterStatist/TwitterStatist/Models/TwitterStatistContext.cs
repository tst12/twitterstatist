﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TwitterStatist.Models
{
    public class TwitterStatistContext : IdentityDbContext<ApplicationUser>
    {
        public TwitterStatistContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }
        public DbSet<AccessToken> AccessTokens { get; set; }
        public DbSet<WorkMode> WorkModes { get; set; }
        public DbSet<Searcher> Searchers { get; set; }
        public DbSet<Tweet> Tweets { get; set; }


        public static TwitterStatistContext Create()
        {
            return new TwitterStatistContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AccessToken>()
        .HasRequired(lu => lu.ApplicationUser);
        }
    }
}

//modelBuilder.Entity<ApplicationUser>().HasOptional(pi => pi.Id);
//    protected override void OnModelCreating(DbModelBuilder modelBuilder)
//    {
//        modelBuilder.Entity<AccessToken>()
//        .HasRequired(lu => lu.ApplicationUser)
//        .WithOptional(pi => pi.AccessToken);
//    }