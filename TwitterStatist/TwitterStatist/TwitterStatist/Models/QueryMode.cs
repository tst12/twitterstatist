﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwitterStatist.Models
{
    enum QMode
    {
       
    }
    public class QueryMode
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Searcher> Searchers { get; set; }

    }
}